import fs = require("fs");
import _ = require("lodash");

namespace IcarusLarm {
    export enum AlarmState { larm, normal, ack, msg };
    export enum ResendAck { none, thisIsAck, seqId, logPoint, physPoint, larmText, anyAck };

    export interface IAlarm {
        systemName: string;     // larmmottagningsdrivern
        systemVersion: number;  // larmmottagningsdrivers version
        received: string;       // YYYY-MM-DD hh:mm:ss
        orginalText: string;    // larmtext såsom den såg ut när den togs emot av GetAlarm eller likn.
        klass: number;          // klass. Alltid 1 om en larmmottagningsdriver, annars 0 (=ingen licens behövs)
        destination: string[];  // destinationer dit larm skall skickas
        destHistory: string[];  // destinationer som larmet besökt, används för att förhindra loop. Rör int.
        ackId: string;          // larmets unika ACK-ID. Används vid återsänding mm. Rör inte.
        data: string;           // intern data. Rör inte.
        larmText?: string;      // larmbeskrivningstext
        extraText?: string;     // tilläggstext som normalt inte visas
        physPoint?: string;     // larmpunkt tekniks adress
        logPoint?: string;      // larmpunkt HMI utseende
        seqId?: string;         // fält för larm-sekvennummer. Används nästgan aldrig
        formatedText?: string;  // Hur larmet skall se ut när det levereras
        state?: AlarmState;     // larmhändelsen/meddelande
        priority?: number;      // 1...n
        dateTime?: string;      // YYYY-MM-DD hh:mm:ss
        resendAck: ResendAck;   // ResendAck.none
        area: string;           // Area, KeyTalk 2017
    }
}


interface ILarmPunkt {
    globalLarmId    :string;    // identifierar en unik larmpunkt (oftast sytemName+'@'+logPoint)
    larm?: IcarusLarm.IAlarm;
    normal?: IcarusLarm.IAlarm;
    ack?: IcarusLarm.IAlarm;
    msg?: IcarusLarm.IAlarm;
    vy: string[];
}

interface IAliveControler {
    setAliveLarm(nod:string,vyer:string[]):void;
    clearAliveLarm(nod:string):void;
}

class Alive {
    public nod: string;
    private liveTill: number;   // utc
    private vy: string[]=[];
    private isLarm: boolean;
    private controler: IAliveControler;
    constructor(controler:IAliveControler) {
        this.isLarm=true;   // Alive skapa när det KOMMER ett kommando, sedan körs proccessCmd() 
                            // och då körs controler.clearAliveLarm()
        this.controler=controler;
    }
    public processCmd(cmd:ICommandAlive) {
        this.nod=cmd.nod;

        if(this.vy.indexOf(cmd.vy)===-1)
            this.vy.push(cmd.vy);
        this.liveTill=new Date().getTime()+cmd.periodicitySeconds*1000*3;
        
        if(this.isLarm) {
            this.isLarm=false;
            this.controler.clearAliveLarm(this.nod);
        }
    }
    public checkNode() {
        if(!this.isLarm && (new Date).getTime() > this.liveTill) {
            this.isLarm=true;
            this.controler.setAliveLarm(this.nod,this.vy);
        }
    }
}



interface ILarmDataRoot {
    larm: ILarmPunkt[];
}

export interface ICommandAdd {
    typ: "add";
    id: string;
    vy?: string;
    larm: IcarusLarm.IAlarm;
}

export interface ICommandAlive {
    typ: "alive";
    id: string;
    nod:string;           // Ifall mer än en IcarusServer så man kan få separata offline
    vy?: string;
    periodicitySeconds: number;
}


export type ICommand = ICommandAdd | ICommandAlive; 

export interface ISvarCmd {
    typ: string;
    id: string;
    error?: string;
}

export interface ILarmOutput {
    globalLarmId        :string;
    state: LarmPunktState;
    tid:number;          
    larm: IcarusLarm.IAlarm;
}

export interface IGetLarmRespons {
    serverStart: number;    // När ändarad - gör en full reload
    now: number;
    larm: ILarmOutput[];
}

enum LarmPunktState { larm,msg,ack,normal,none }; 

let parseDT=(s:string)=> s?( Date.parse(s).valueOf() ):0;
let getDT=(x:IcarusLarm.IAlarm)=> parseDT( x?(x.received||undefined):undefined );
let getDTsort=(x:IcarusLarm.IAlarm)=> parseDT( x?(x.dateTime||x.received||undefined):undefined );

function dateTimeToString(x:Date) {
    let s;

    let mo = (x.getMonth() + 1).toString();
    let da = x.getDate().toString();
    if (mo.length === 1) {
        mo = "0" + mo;
    }
    if (da.length === 1) {
        da = "0" + da;
    }

    s = x.getFullYear().toString() + "-" + mo + "-" + da;

    let hr = x.getHours().toString();
    if (hr.length === 1) {
        hr = "0" + hr;
    }
    let mn = x.getMinutes().toString();
    if (mn.length === 1) {
        mn = "0" + mn;
    }
    let se = x.getSeconds().toString();
    if (se.length === 1) {
        se = "0" + se;
    }
    s = s + " " + hr + ":" + mn + ":" + se;
    return s;
}


const maxAntalIDB=1000;
const removeDoneOlderMsek = (24*60*60*1000); // Kvitterade och återställda (spara ifall Icarus återsänder)
const removeAckOlderMsek = (2*24*60*60*1000); // Kvitterade men ej återgått
const removeAnyOlderMsek = (7*24*60*60*1000); // Vad som helst som legat längre tid       


class LarmPunkt {
    data:ILarmPunkt;
    sortDT:number;
    newest:number;
    state:LarmPunktState;
    constructor(data:ILarmPunkt) {
        this.data=data;
        this.calcState();
    }
    private calcState() {
        let larmDT=getDT(this.data.larm);
        let normalDT=getDT(this.data.normal);
        let ackDT=getDT(this.data.ack);
        let msgDT=getDT(this.data.msg);
        this.newest= _.max( [larmDT,msgDT,normalDT,ackDT,0] );

        if(larmDT>normalDT && larmDT>ackDT) {
            this.state=LarmPunktState.larm;
            this.sortDT= larmDT;
        } else if(msgDT>ackDT) {
            this.state=LarmPunktState.msg;
            this.sortDT= msgDT;
        } else if(normalDT>larmDT && larmDT>ackDT) {
            this.state=LarmPunktState.normal;
            this.sortDT= larmDT || normalDT;
        } else if(ackDT>larmDT && larmDT>normalDT && !msgDT ) {
            this.state=LarmPunktState.ack;
            this.sortDT= ackDT;
        } else {
            this.sortDT= this.newest;
            this.state=LarmPunktState.none;
        }
    }
    public wantRemoveFromDB(tid:number) {
        console.log(new Date(this.newest).toLocaleString());
        console.log(this.state);
        if(this.state===LarmPunktState.none && this.newest<(tid-removeDoneOlderMsek))
             return true;
        if(this.state===LarmPunktState.ack && this.newest<(tid-removeAckOlderMsek))
             return true;
         else if(this.newest<(tid-removeAnyOlderMsek))
             return true;
        return false;        
    }
    addLarm(l:IcarusLarm.IAlarm,vy:string):boolean {
        let chg=false;
        let currentInDB=<IcarusLarm.IAlarm> ((<any>this.data)[IcarusLarm.AlarmState[l.state]]);
        if(currentInDB) {
            // Ny vy?
            chg = chg || this.data.vy.indexOf(vy)===-1;
            // Är den vi har för akuellt larmtillstånd nyare?
            chg=chg || l.received>currentInDB.received;

            if(chg && (l.state===IcarusLarm.AlarmState.larm || l.state===IcarusLarm.AlarmState.msg)) {
                // Om "utlöst", ta bort ev ACK/NORMAL som är äldre (så blir DB mindre)
                if(this.data.ack && (this.data.ack.received<l.received))
                    this.data.ack=undefined;
                if(this.data.normal && (this.data.normal.received<l.received))
                    this.data.normal=undefined;
            }
        } else {
            chg=true;
        }
        if(chg) {
            if(this.data.vy.indexOf(vy)===-1)
                this.data.vy.push(vy);
            ((<any>this.data)[IcarusLarm.AlarmState[l.state]])=l;
            this.calcState();
        }
        return chg;
    }
    public getLarmForDisplay():ILarmOutput {
        let ls=[this.data.larm,this.data.msg,this.data.normal,this.data.ack].filter(x=>!!x);
        let nyastHandelse=_.maxBy(ls,x=>x.received);

        return {
            globalLarmId: this.data.globalLarmId,
            state: this.state,
            tid: this.sortDT,
            larm: nyastHandelse
        }
    }
}

export class LarmData implements IAliveControler {
    rawdata: ILarmDataRoot;
    larm: LarmPunkt[];
    alives: Alive[] = []; 
    dirty: boolean = false;
    serverStart: number;

    constructor() {
        this.serverStart=new Date().valueOf();
        console.log("laddar sparad data");
        try {
            this.rawdata=JSON.parse( fs.readFileSync("larmDB.json","utf8") );
            //console.log(this.rawdata);
        }
        catch(ex)
        {
            console.log("fel när laddar: "+ex);
            this.rawdata={
                larm:[]
            }
        }
        console.log("det fanns "+this.rawdata.larm.length+" larm");
        this.larm=this.rawdata.larm.map(x=> new LarmPunkt(x));
    }

    setAliveLarm(nod:string,vyer:string[]):void {
        let gid="alive."+nod;
        let lp=_.find(this.larm,x=>x.data.globalLarmId===gid);
        if(!lp) {
            lp=new LarmPunkt({
                globalLarmId: gid,
                vy: _.clone(vyer), 
            });
            this.larm.push(lp);
            let tid=dateTimeToString(new Date());
            let pri=2;
            let msg="Ingen kontakt med larmhanteringsservern Icarus";
            let formatedmsg=`PRI ${pri} UTLÖST    *** ${tid}
${nod}
${msg}`;
            let larm:IcarusLarm.IAlarm = {
                ackId: null,
                destination: null,
                area: null,
                extraText: null,
                data: null,
                destHistory: null,
                physPoint: null,
                seqId: null,
                klass: 0,
                dateTime: tid,
                logPoint: nod,
                resendAck: IcarusLarm.ResendAck.none,
                state: IcarusLarm.AlarmState.larm,
                systemName: "www",
                systemVersion: 1,
                formatedText: formatedmsg,
                larmText: msg,
                orginalText: formatedmsg,
                priority: pri,
                received: tid
            };
            lp.addLarm(larm,vyer[0]);
        }
    }
    clearAliveLarm(nod:string):void {
        let gid="alive."+nod;
        let lp=_.find(this.larm,x=>x.data.globalLarmId===gid);
        if(lp) {
            this.dirty=true;
            this.larm=this.larm.filter(x=> x!==lp);
        }
    }

    private trimData() {
        console.log("trimData");
        let tid=(new Date()).valueOf();
        this.larm=this.larm.filter(x=> !x.wantRemoveFromDB(tid));
        while(this.larm.length>maxAntalIDB) {
            let urval=this.larm.filter(x=>x.state===LarmPunktState.none);
            if(urval.length===0)
                urval=this.larm.filter(x=>x.state===LarmPunktState.ack);
            if(urval.length===0)
                urval=this.larm;
            let lp=_.minBy(urval,x=>x.sortDT);
            this.larm=this.larm.filter(x=> x!==lp);
            console.log("forced trim on: "+lp.data.globalLarmId+", state="+LarmPunktState[lp.state]+", dt="+(new Date(lp.sortDT)).toString());
        }
    }

    public save() {
        if(!this.dirty)
            return;
        this.dirty=false;
        this.trimData();
        console.log("sparar");
        this.rawdata.larm=this.larm.map(x=>x.data);
        fs.writeFileSync( "larmDB.json", JSON.stringify( this.rawdata,undefined,4 ), "utf8");
    }

    private doCommandAdd(cmd:ICommandAdd):ISvarCmd {
        let glid=cmd.larm.systemName+"@"+(cmd.larm.logPoint||cmd.larm.physPoint||"crazy");
        let lp=_.find(this.larm,x=>x.data.globalLarmId===glid);
        if(!lp) {
            lp=new LarmPunkt( {
                globalLarmId: glid,
                vy: []
            });
            this.larm.push(lp);
        }
        if(lp.addLarm(cmd.larm,cmd.vy||""))
            this.dirty=true;
        return {
            id: cmd.id,
            typ: cmd.typ
        }
    }
    private doCommandAlive(cmd:ICommandAlive):ISvarCmd {
        let ns=this.alives.filter(x=>x.nod===cmd.nod);
        let n=ns && ns[0];
        if(!n) {
            n=new Alive(this);
            this.alives.push(n);
        }
        n.processCmd(cmd);
        return {
            id: cmd.id,
            typ: cmd.typ
        }
    }

    public doCommand(cmd:ICommand):ISvarCmd {
        if(cmd.typ==="add")
            return this.doCommandAdd(cmd);
        else if(cmd.typ==="alive")
            return this.doCommandAlive(cmd);
        else
            return {
                id: (<any>cmd).id,
                typ: (<any>cmd).typ,
                error: "unknown typ"
            };
    }

    public remove(glarmid:string) {
        let lp=_.find(this.larm,x=>x.data.globalLarmId===glarmid);
        if(lp) {
            this.larm=this.larm.filter(x=> x!==lp);
            this.dirty=true;
        }        
    }

    public getLarm(vy:string):IGetLarmRespons {
        return {
            serverStart: this.serverStart,
            now: (new Date()).getTime(),
            larm:this.larm
                //.filter(x=>x.state!==LarmPunktState.none)
                .filter(x=>x.data.vy.indexOf(vy)!==-1)
                .map(x=>x.getLarmForDisplay())

        }

    }

    public checkAlives() {
        this.alives.forEach(x=> x.checkNode());
    }


}
