import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import * as session from "express-session";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import larmData = require("./larmdata");


//import { IndexRoute } from "./routes/index";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

  public app: express.Application;
  public data: larmData.LarmData;

  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
   */
  public static bootstrap(): Server {
    return new Server();
  }

  /**
   * Constructor.
   *
   * @class Server
   * @constructor
   */
  constructor() {
    this.data=new larmData.LarmData();

    //create expressjs application
    this.app = express();

    //configure application
    this.config();

    //add routes
    //this.routes();

    //add api
    this.api();
  }


  private requireLogin(req: express.Request, res: express.Response, next: express.NextFunction) {
    //next();
    if (req.session.authenticated) {
      next(); // allow the next route to run
    } else {
      //res.render('unauthorised', { status: 403 });      
      // require the user to log in
      req.session.starturl=req.url;
      res.redirect("/login.html"); // or render a form, etc.
    }
  }

  /**
   * Create REST API routes
   *
   * @class Server
   * @method api
   */
  public api() {
    //empty for now
  }

  /**
   * Configure application
   *
   * @class Server
   * @method config
   */
  public config() {

    //configure pug
    // this.app.set("views", path.join(__dirname, "views"));
    // this.app.set("view engine", "pug");

    //mount logger
    this.app.use(logger("dev"));

    //mount json form parser
    this.app.use(bodyParser.json());

    //mount query string parser
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));

    //mount cookie parker
    this.app.use(cookieParser("JLBBMGJG_SECRET_GOES_HERE"));

    // session
    this.app.use(session({ 
      saveUninitialized: false,
      name:"icarusNode.sid",
      secret: 'JLBBMGJG_SECRET_GOES_HERE' 
    }));

    //mount override?
    this.app.use(methodOverride());

    // catch 404 and forward to error handler
    this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
        err.status = 404;
        next(err);
    });

    //error handling
    this.app.use(errorHandler());

    this.app.all("/admin", function(req, res, next) {
      res.redirect("login.html");
    });

    let passw=process.env.PASSWORD || "admin";
    this.app.post("/login.html", function(req, res,next) {
      console.log("login: "+req.body.username
        //"  "+req.body.password
      );
  		if (req.body.user && req.body.user === 'admin' && req.body.password && req.body.password === passw) {
        req.session.authenticated = true;
        res.redirect(req.session.starturl || '/admin.html');
      }
    });

    this.app.all("/admin/*", this.requireLogin, function(req, res, next) {
      next();
    });
    this.app.all("/admin.html", this.requireLogin, function(req, res, next) {
      next();
    });

    this.app.post("/admin/logout", function(req, res, next) {
      console.log("log out");
        req.session.authenticated = false;
        (<any>req).session.save();
    });

    this.app.post('/admin/remove',(req,res)=> {
      let gid=req.body.gid;
      console.log("remove "+gid);
      this.data.remove(gid);
      res.send(JSON.stringify(null));      
    });


    // Statiska filer
    this.app.use(express.static(path.join(__dirname, "public"),{
      // Standardsida
      index: "index.html"
    }));

    this.app.get('/larm',(req,res)=> {
      let vy=req.query.vy || "";
      //console.log("get: "+vy)
      res.set('Content-Type', 'application/json');
      this.data.checkAlives();
      this.data.save();
      let svar=this.data.getLarm(vy);
      //console.log(svar);
      res.send(JSON.stringify(svar))
    });

    let tmp=(""+(process.env.ICARUSIP || ""));
    let icarusIP=tmp? (""+(process.env.ICARUSIP || "")).split(",") : null;

    this.app.post('/icarus',(req,res)=> {
      //console.log("Got a POST request" );
      //console.log("Got a POST request - "+req.body );
      //console.log(req.body);

      var ip = (req.headers['x-forwarded-for'] || 
          req.connection.remoteAddress || 
          req.socket.remoteAddress ||
          <string>((<any>req).connection.socket.remoteAddress) ||
          "").split(',')[0].trim();

      // Begränsa /icarus till Icarus Server IP-adress
      if(icarusIP && !icarusIP.some(x=>x===ip) ) {
        res.render('unauthorised', { status: 403 });
        return;      
      }

      console.log(ip+" post:"+JSON.stringify(req.body));
      let svar=this.data.doCommand(req.body);
      this.data.save();
      console.log("svar: "+JSON.stringify(svar));
      res.send(JSON.stringify(svar));
    });


  }

  /**
   * Create and return Router.
   *
   * @class Server
   * @method config
   * @return void
   */
  // private routes() {
  //   let router: express.Router;
  //   router = express.Router();

  //   //IndexRoute
  //   IndexRoute.create(router);

  //   //use router middleware
  //   this.app.use(router);
  // }

}