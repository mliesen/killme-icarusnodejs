module.exports = function(grunt) {
  "use strict";

  grunt.initConfig({
    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: "./public",
            src: ["**"],
            dest: "./dist/public"
          }
          // {
          //   expand: true,
          //   cwd: "./views",
          //   src: ["**"],
          //   dest: "./dist/views"
          // }
        ]
      },
      webapp: {
        files: [
          {
            expand: true,
            cwd: "./src-webapp",
            src: ["**"],
            dest: "./dist/public/src-webapp"
          },
        ]
      }
    },
      less: {
          dist: {
              options: {
                  //paths: ["importfolder"]
              },
              files: [
                  {
                      "src-webapp/icarus.css": "src-webapp/icarus.less"
                  },
              ]
          }
      },

    ts: {
      app: {
        files: [{
          src: ["src-server/\*\*/\*.ts", "!src-server/.baseDir.ts"],
          dest: "./dist"
        }],
        options: {
          module: "commonjs",
          target: "es6",
          sourceMap: true
        }
      },
      webapp: {
        files: [{
          src: ["src-webapp/\*\*/\*.ts", "!src-webapp/.baseDir.ts"]
          // ,dest: "./dist/public/src-webapp"
        }],
        options: {
          module: "none",
          //module: "commonjs",
          target: "es5",
          noImplicitAny: true,
          sourceMap: true
        }
      }
    },
    watch: {
      ts: {
        files: ["src-server/\*\*/\*.ts"],
        tasks: ["ts"]
      },
      views: {
        files: ["views/**/*.pug"],
        tasks: ["copy"]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-ts");
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask("default", [
    "ts",
    "less",
    "copy"
  ]);

};