# IcarusNodeJS
Aktiva larm på TV-skärm. NodeJS server kopplad till Icarus
https://github.com/mliesen/IcarusNodeJS

För att köra:
-------------
npm install
set port=8080
set password=admin
set icarusip=127.0.0.1
start npm run start


Enviroment variabler som bör sättas
-----------------------------------
PORT      (default 8080)
PASSWORD  (default admin)
ICARUSIP  (default tomt för att tillåta alla. 
                Fler kan anges kommaseparerat.
                Exempel. 
                SET ICARUSIP="192.168.10.10,127.0.0.1"  )
