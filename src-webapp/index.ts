namespace IcarusLarm {
    export enum AlarmState { larm, normal, ack, msg };
    export enum ResendAck { none, thisIsAck, seqId, logPoint, physPoint, larmText, anyAck };

    export interface IAlarm {
        systemName: string;     // larmmottagningsdrivern
        systemVersion: number;  // larmmottagningsdrivers version
        received: string;       // YYYY-MM-DD hh:mm:ss
        orginalText: string;    // larmtext såsom den såg ut när den togs emot av GetAlarm eller likn.
        klass: number;          // klass. Alltid 1 om en larmmottagningsdriver, annars 0 (=ingen licens behövs)
        destination: string[];  // destinationer dit larm skall skickas
        destHistory: string[];  // destinationer som larmet besökt, används för att förhindra loop. Rör int.
        ackId: string;          // larmets unika ACK-ID. Används vid återsänding mm. Rör inte.
        data: string;           // intern data. Rör inte.
        larmText?: string;      // larmbeskrivningstext
        extraText?: string;     // tilläggstext som normalt inte visas
        physPoint?: string;     // larmpunkt tekniks adress
        logPoint?: string;      // larmpunkt HMI utseende
        seqId?: string;         // fält för larm-sekvennummer. Används nästgan aldrig
        formatedText?: string;  // Hur larmet skall se ut när det levereras
        state?: AlarmState;     // larmhändelsen/meddelande
        priority?: number;      // 1...n
        dateTime?: string;      // YYYY-MM-DD hh:mm:ss
        resendAck: ResendAck;   // ResendAck.none
        area: string;           // Area, KeyTalk 2017
    }
}

interface ILarmOutput {
    globalLarmId        :string;
    state: LarmPunktState;
    tid:number;          
    larm: IcarusLarm.IAlarm;
}

interface IGetLarmRespons {
    serverStart: number;    // När ändarad - gör en full reload
    now: number;
    larm: ILarmOutput[];
}

enum LarmPunktState { larm,msg,ack,normal,none }; 

function dateToStringDDMM(d:Date):string {			// datumdelean av date i localtime omvandlas till sträng DD/MM
    //let y = (d.getFullYear()).toString();
    let m = (d.getMonth() + 1).toString();
    let d2 = d.getDate().toString();
    if (m.length === 1) {
        m = "0" + m;
    }
    if (d2.length === 1) {
        d2 = "0" + d2;
    }
    return d2 + "/" + m;
}


class App {
    public admin:boolean;
    public maxAntal=20;
    private timer:number;
    private lastServerStart:number;
    private periodicUpdate:number;
    private getLarmUrl:string;
    private lastRespons:number;
    private lastResponsServerTime:number;
    private lastTheme:number=-1;
    private isOnline:boolean=false;
    private isTrunced:boolean=false;
    private $maindiv:JQuery;
    private lastLarm:ILarmOutput[];

    private fullReload() {
        if(this.timer) {
            window.clearInterval(this.timer);
            this.timer=undefined;
        }
        try {
            window.location.reload(true);
        } catch (e) {
            window.location.reload();
        }
    }

    private checkOnline() {
        let isOnline=(new Date).getTime() < (this.lastRespons+(30*1000));
        if(this.isOnline!==isOnline) {
            this.isOnline=isOnline;
            this.$maindiv.toggleClass("wwwoffline",!this.isOnline);
        }
    }



    private skapaLarmRad(larm:ILarmOutput) {
        let system=larm.larm.systemName;
        if(system==="Server Test")
            system="Testmeddeln.";
        if(system==="KeyTalk1")
            system="KeyTalk";
        if(system.toLowerCase()==="tavista")
            system="Vista";
        //let tidStr=KeyLib.DateTimeEx.HumanReadableTime(new Date(larm.tid));
        let dt=new Date(larm.tid);
        let tidStr=dateToStringDDMM(dt)+" "+KeyLib.DateTimeEx.timeToString(dt,false);
        KeyLib.DateTimeEx.dateToString
        let tag=larm.larm.logPoint || larm.larm.physPoint || "";
        tag=tag.replace("\\Root\\","\\").replace("\\FirstDevice\\","\\").replace("-\\","\\").replace("\\alarm","").replace("\\MAIN.","\\");
        let txt=larm.larm.larmText||larm.larm.formatedText||larm.larm.orginalText

        if(larm.larm.systemName==="KeyTalk1") {
            // Oftast är HMI larm ID det första "ordet" i larmtexten
            let split=txt.split(" ");
            let first=split[0];
            if(first.indexOf("-")!==-1 || first.indexOf(".")) {
                tag=first;
                txt=_.drop(split,1).join(" ");
            }
        }
        let status="";
        switch(larm.state) {
            case LarmPunktState.larm:
            case LarmPunktState.msg:
                status="Utlöst";
                break;
            case LarmPunktState.normal:
                status="Återst.";
                break;
            case LarmPunktState.ack:
                status="Kvitt.";
                break;
            case LarmPunktState.none:
                status="Klar";
                break;
        }
        if(this.admin) {
            status=`<button name='${KeyLib.Text.htmlAttributeEncode(larm.globalLarmId)}'>Ta bort</button>`;
        }

        // HACK:  Inga space/radbrytnigar mellan inline-divs. Då blir det ca 4 pixlar mellan...
        let html=`<li class='${LarmPunktState[larm.state]}'><!--
                --><div class='tid'>${tidStr}</div><!--
                --><div class='space'></div><!--
                --><div class='system'>${system}</div><!--
                --><div class='space'></div><!--
                --><div class='tag'>${tag}</div><!--
                --><div class='space'></div><!--
                --><div class='text'>${KeyLib.Text.htmlEncode(_.trim( txt)) }</div><!--
                --><div class='space'></div><!--
                --><div class='status'>${status}</div></li>`;

        return html;
    }

    private orderLarm(larms:ILarmOutput[]):ILarmOutput[] {
        return _(larms)
            .orderBy( (x:ILarmOutput)=>x.tid)
            .orderBy( (x:ILarmOutput)=>x.state)
            .value();
    }

    private updateList(larms:ILarmOutput[]) {
        // Ta utlöst & återställda
        let viktigaste=
            _.orderBy(       
                larms.filter(x=>x.state===LarmPunktState.larm || x.state===LarmPunktState.msg ),
                x=>-x.tid
            );
        // Får dom plats? Om inte klipp
        let isTrunced=viktigaste.length>this.maxAntal;
        if(isTrunced)
            viktigaste=_.take(viktigaste,this.maxAntal);

        // ta fram kvitterade/avklarade
        let normala=
            _.orderBy(       
                larms.filter(x=>x.state===LarmPunktState.normal ),
                x=>-x.tid
            );

        let kvitterade=
            _.orderBy(       
                larms.filter(x=>x.state===LarmPunktState.ack ),
                x=>-x.tid
            );
             
        let avklarade=
            _.orderBy(       
                larms.filter(x=>x.state===LarmPunktState.none ),
                x=>-x.tid
            );

        // ta fram det som skall visas
        let ls = _.take( _.concat(viktigaste,kvitterade,normala,avklarade), this.maxAntal);

        // Ordna enligt aktuell tillstånd
        ls=_.orderBy(ls, x=> (x.state===LarmPunktState.msg)?LarmPunktState.larm:x.state );


        if(this.isTrunced!==isTrunced) {
            this.isTrunced=isTrunced;
            this.$maindiv.toggleClass("trunced",!this.isTrunced);
        }

        let ss=ls.map(x=> this.skapaLarmRad(x));

        $("ul",this.$maindiv).html(ss.join(""));
    }

    private checkTheme() {
        let timma=(new Date).getHours();
        let theme=KeyLib.MathEx.divInt(timma,6);
        if(theme!==this.lastTheme) {
            this.lastTheme=theme;
            $("body")
                .removeClass("theme0")
                .removeClass("theme1")
                .removeClass("theme2")
                .removeClass("theme3")
                .addClass("theme"+theme);
        }           
    }

    private doTimer() {
        Q($.getJSON("larm")).then( (data:IGetLarmRespons)=> {
            if(this.lastServerStart) {
                if(this.lastServerStart!==data.serverStart) {
                    this.fullReload();
                    return;
                }
            }
            this.lastServerStart=data.serverStart;
            let nu=(new Date).getTime();
            this.lastRespons=nu;
            this.lastResponsServerTime=data.now;

            if(!this.lastLarm || !_.isEqual(data.larm,this.lastLarm) ||  nu>this.periodicUpdate ) {
                this.periodicUpdate=nu+60*1000; // tider visas som "för 0 minuter sedan" - så vi måste uppdatera varje minut
                this.lastLarm=data.larm;
                this.updateList(data.larm);
            }
            this.checkOnline();
        });
        this.checkOnline();
        this.checkTheme();
        this.updateKlocka();
    }

    updateKlocka() {
        let tid=(new Date()).getTime();
        if(this.lastResponsServerTime) {
            let delta=tid-this.lastRespons;
            tid=this.lastResponsServerTime+delta;
        }
        let txt=KeyLib.DateTimeEx.timeToString(new Date(tid),false);
        $("#klocka",this.$maindiv).text(txt);
    }

    constructor(vy:string,$div:JQuery,admin:boolean) {
        this.$maindiv=$div;
        this.admin=admin;
        if(admin)
            this.maxAntal=9999;
        this.$maindiv.append($(`
            <div class="offlineWarning">Kan inte nå larmserver - larmlistan kan vara inaktuell</div>
            <div id='header'>
                ${this.admin?'<button id="logout">Logga ut</button>':''}
                <div id="klocka"></div>
                <h1>Larmtablå - viktigaste larmen</h1>
            </div>
            <div id='larm'>
                <ul></ul>
            </div>
            <div id='footer'>
            </div>
            <div class="truncedWarning">Det finns fler larm som inte kan visas, se respektive fastighetsystem.</div>
        `));
        if(vy)
            this.$maindiv.addClass("VY_"+vy);
        this.getLarmUrl='larm';
        if(vy)
            this.getLarmUrl=this.getLarmUrl+"?vy="+vy;

        if(admin) {
            $("ul",this.$maindiv).click(ev=>{
                if(ev.target.nodeName==='BUTTON')
                {
                    let gid=$(ev.target).attr("name");
                    console.log("remove "+gid);
                    (<HTMLInputElement>(ev.target)).disabled=true;
                    Q($.post("admin/remove",{ gid:gid })).then(()=>{
                        this.doTimer();
                    });
                }
            });
            $("#logout",this.$maindiv).click(ev=>{
                Q($.post("admin/logout")).then(()=>{
                    window.location.href='index.html';
                    // do nothing
                    //this.doTimer();
                });
            });
        }

        this.doTimer();
        this.timer=window.setInterval(()=>this.doTimer(),10000);
    }
}
