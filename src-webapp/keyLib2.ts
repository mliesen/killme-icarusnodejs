﻿namespace KeyLib {
    export namespace MathEx {
        export function divInt(a:number, b:number):number {
            return (a - a % b) / b;
        }
    }

    export namespace web {
        export function getUrlParams(): any {
            let vars:any= {};
            let s=decodeURIComponent(window.location.href.replace(/\+/g, '%20'));
            let n=s.indexOf('?');
            if(n>=0) {
                let hashes = s.slice(n + 1).split('&');
                for (let i = 0; i < hashes.length; i++) {
                    let hash = hashes[i].split('=');
                    if (hash.length === 1) {
                        vars[hash[0]] = null;
                    } else {
                        if (hash.length > 1) {
                            vars[hash[0]] = hash[1];
                        }
                    }
                }
            }
            return vars;
        }
    }

    export namespace Base64 {
        let _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

        export function decode(input:string):ArrayBuffer {
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            let bytes = Math.round((input.length / 4) * 3);

            // minska längd ifall padding
            if(input.charAt(input.length - 1)==="=")
                bytes--;
            if(input.charAt(input.length - 2)==="=")
                bytes--;

            let chr1:number, chr2:number, chr3:number;
            let enc1:number, enc2:number, enc3:number, enc4:number;
            let i = 0;
            let j = 0;
            let ab = new ArrayBuffer(bytes);

            let uarray:Uint8Array=new Uint8Array(ab);

            for (i=0; i<bytes; i+=3) {
                //get the 3 octects in 4 ascii chars
                enc1 = _keyStr.indexOf(input.charAt(j++));
                enc2 = _keyStr.indexOf(input.charAt(j++));
                enc3 = _keyStr.indexOf(input.charAt(j++));
                enc4 = _keyStr.indexOf(input.charAt(j++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                uarray[i] = chr1;
                if (enc3 !== 64)
                    uarray[i+1] = chr2;
                if (enc4 !== 64)
                    uarray[i+2] = chr3;
            }
            return ab;
        }
    }

    export namespace FloatEx {
        export function isNumber(n:any):boolean {
            // http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric
            return !isNaN(parseFloat(n)) && isFinite(<any>n);
        }
        export function parseFloatEx(txt:string):number {
            if (txt === null || txt === undefined || isNaN(<any>txt)) {
                return <any>txt;
            }
            return parseFloat(txt.replace(",", "."));
        };
    }

    export namespace Text {
        export function leftpad(s: (string|number), size: number,c?:string) {
            let r = s + "";
            if (!c)
                c = "0";
            while (r.length < size) r = c + r;
            return r;
        }

        // ersätter alla olika {xxx} med värdet av obj.xxx
        // om obj saknar xxx så ersätts med ""
        // obj kan även vara en funktion som tar en sträng och returnerar en sträng eller object.
        // Ersättning sker även rekursivt i några nivåers djup. (Ifall obj.xxx i sin tur innehåller {yyy})
        export function templateString(text:string,obj:object|( (lookup:string) =>string),lev?:number) {
            if(!text)
                return text;
            if(!lev)
                lev=1;
            if(lev>4)
                return text;    // unvika rekursivt ej terminerande
            let state=0;
            let mark=0;
            let n=0;
            let svar:string[]=[];
            let len=text.length;
            while(n<=len) {
                switch(state) {
                    case 0:
                        if(n===len || text.charAt(n)==="{") {
                            svar.push(text.substring(mark,n));
                            mark=n+1;
                            state=1;
                        }
                        break;
                    case 1:
                        if(text.charAt(n)==="}") {
                            let solve=text.substring(mark,n);
                            let ny:string=undefined;
                            if(typeof obj === "function") {
                                ny=obj(solve);
                            } else {
                                ny=((<any>obj)[solve]).toString();
                            }
                            if(ny)
                                svar.push(  templateString(ny,obj,lev+1));
                            mark=n+1;
                            state=0;
                        }
                        break;
                    default:
                        throw "impossible";
                }
                n++;
            }
            return svar.join("");
        }


        export function compareStringWithNumeric(a: string, b: string): number {
            let aa = findNumeric(a);
            let bb = findNumeric(b);
            if (aa.index !== bb.index || aa.index === -1 || bb.index === -1) {
                return a.localeCompare(b);
            } else {
                if (aa.index === 0) {
                    let an = parseInt(a.substr(aa.index, aa.len), 10);
                    let bn = parseInt(b.substr(bb.index, bb.len), 10);
                    let svar = an - bn;
                    if (svar !== 0)
                        return svar;
                    return compareStringWithNumeric(a.substr(aa.index + aa.len), b.substr(bb.index + bb.len));
                } else {
                    let svar = a.substr(0, aa.index).localeCompare(b.substr(0, bb.index));
                    if (svar !== 0)
                        return svar;
                    return compareStringWithNumeric(a.substr(aa.index), b.substr(bb.index));
                }
            }
        }

        export function findNumeric(s: string): { index: number; len: number } {
            if (!s) {
                return { index: -1, len: 0 };
            }
            let t: number = 1;
            if (s.charAt(0) >= "0" && s.charAt(0) <= "9") {
                while (t < s.length && (s.charAt(t) >= '0') && (s.charAt(t) <= '9')) {
                    t++;
                }
                return { index: 0, len: t  };
            } else {
                while (t < s.length && !((s.charAt(t) >= '0') && (s.charAt(t) <= '9'))) {
                    t++;
                }
                if (t === s.length) {
                    return { index: t, len: 0 };
                } else {
                    let i = t;
                    while (t < s.length && (s.charAt(t) >= '0') && (s.charAt(t) <= '9')) {
                        t++;
                    }
                    return { index: i, len: t-i };
                }
            }
        }

        export function parseKeyEnum(str:string):string[] {
            // Svar fås som en array (JS array är ju sparse)
            // använd t.ex. n=_.indexOf(enumArr, txt, false);
            //              txt=enumArr[n]
            let svar:string[] = [];
            let lastNr = -1;
            $.each(str.split(";"), function () {
                let n = this.indexOf("=");
                if (n !== -1) {
                    let tals = $.trim(this.substr(0, n));
                    let intRegex = /^[\-]?\d+$/;
                    if (intRegex.test(tals)) {
                        lastNr = parseInt(tals, 10);
                    } else {
                        n = -1;
                    }
                }
                if (n === -1) {
                    lastNr++;
                    svar[lastNr] = this.toString();
                } else {
                    svar[lastNr] = $.trim(this.substr(n + 1)).toString();
                }
            });
            return svar;
        };

        export function dbgenEnum2KeyEnum(str: string) {
            if (str.indexOf(',') === -1 || str.indexOf(';') !== -1) {
                return str;
            }
            // 0,A,B,27=Z    =>   A;B;27=Z
            let ny:string[]=[];
            let ss=str.split(",");
            let atN = 0;
            let atNy = 0;

            for (let n = 0; n < ss.length; n++) {
                let s = ss[n];
                if (n === 0 && FloatEx.isNumber(s)) {
                    atN = parseInt(ss[n], 10);
                } else {
                    let i1 = s.indexOf("=");
                    if (i1 !== -1) {
                        let r = s.substr(0, i1);
                        if (FloatEx.isNumber(r)) {
                            atN = parseInt(r, 10);
                            s = s.substr(i1+1);
                        }
                    }
                    if (atNy === atN) {
                        ny.push(s);
                    } else {
                        ny.push(atN + "=" + s);
                    }
                    atN = atN + 1;
                    atNy = atN;
                }
            }
            return ny.join(";");
        }


        export function strToIntEx(s:string):number {
            if (s.length > 2) {
                if (s[0] === "$") {
                    return parseInt(s.substr(1), 16);
                } else if (s.substr(0, 2) === "0x") {
                    return parseInt(s.substr(2), 16);
                } else if (s[s.length - 1] === "b") {
                    return parseInt(s.substr(0, s.length - 2), 2);
                }
            }
            return parseInt(s, 10);
        };

        export function htmlEncode(s:string):string {
            // gör text säker för html
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            } else {
                return s;
            }
        };
        export function htmlEncodeWithBR(s: string): string {
            // gör text säker för html
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\r/g, '<br/>');
            } else {
                return s;
            }
        };

        export function htmlAttributeEncode(s:string):string {
            // gör text säker för html
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, "&quot;").replace(/'/g, '&#34;');
            } else {
                return s;
            }
        };

        export function jsStringEncode(s:string):string {
            if (s) {
                return s.replace(/\\/g, '\\\\');
            } else {
                return s;
            }
        };

        export function remove(s:string, indx:number, len:number):string {
            let svar = '';
            if (indx > 0) {
                svar = s.substring(0, indx);
            }
            if (indx + len < s.length) {
                svar += s.substring(indx + len, s.length);
            }
            return svar;
        }


        // TODO: Ta bort dessa - de finns ju i lodash
        export function startsWith(small: string, s: string): boolean {
            return _.startsWith(s, small);
            //return (s || "").lastIndexOf(small, 0) === 0;
        };
        // TODO: Ta bort dessa - de finns ju i lodash
        export function endsWith(small: string, s: string): boolean {
            return _.endsWith(s, small);
            //return (s || "").indexOf(small, s.length - small.length) !== -1;
        };
        // TODO: Ta bort dessa - de finns ju i lodash
        export function contains(small: string, s: string) {
            return _.includes(s, small);
            //return (s || "").indexOf(small) !== -1;
        }

    }

    export namespace FloatEx {
        // Finn information ur sträng innehållande ett flyttal.
        // Inledande och avslutande whitespace är tillåtet.
        // Punkt och kommatecken är tillåtet. Litet e eller stort.
        // AscII 160 är tillåten som tusenavskilljare

        interface IFloatBroken {
            vsiffror: number;     // Antal värdesiffror som hittats. (om valid==true)
            exp: number;     // Exponent (värdet) (om valid==true)
            helAntal: number;     // Antal siffror innan decimalavskilljare. Inledande nollor räknas inte.  (om valid==true)
            decAntal: number;     // Antal siffror efter decimalavskilljare  (om valid==true)
            valid: Boolean;  // Indikerar om korrekt flytttal
        }

        export function breakStrFloat(s: string): IFloatBroken {
            let svar = {
                vsiffror: 0,     // Antal värdesiffror som hittats. (om valid==true)
                exp: 0,     // Exponent (värdet) (om valid==true)
                helAntal: 0,     // Antal siffror innan decimalavskilljare. Inledande nollor räknas inte.  (om valid==true)
                decAntal: 0,     // Antal siffror efter decimalavskilljare  (om valid==true)
                valid: false  // Indikerar om korrekt flytttal
            };
            let STATE = {
                INITWHITE: 0,
                TALSIGN: 1,
                TAL: 2,
                DOT: 3,
                DOTTAL: 4,
                EXPO: 5,
                EXPSIGN: 6,
                EXPTAL: 7,
                SLUT: 8
            };

            let mode = STATE.INITWHITE;
            let expMinus = false;
            let t;
            let c;
            for (t = 0; t < s.length; t++) {
                c = s.charAt(t);
                switch (mode) {
                    case STATE.INITWHITE:
                        if (c >= '0' && c <= '9') {
                            if (c !== '0') {
                                svar.helAntal = 1;
                            }
                            mode = STATE.TAL;
                        } else if (c === '+' || c === '-') {
                            mode = STATE.TALSIGN;
                        } else if (c !== '\t' && c !== ' ' && c !== '\r' && c !== '\n' && c !== '\xa0') {
                            return svar;
                        }
                        break;
                    case STATE.TALSIGN:
                        if (c >= '0' && c <= '9') {
                            if (c !== '0') {
                                svar.helAntal = 1;
                            }
                            mode = STATE.TAL;
                        } else {
                            return svar;
                        }
                        break;
                    case STATE.TAL:
                        if (c === '.' || c === ',') {
                            mode = STATE.DOT;
                        } else if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                        } else if (c === 'e' || c === 'E') {
                            mode = STATE.EXPO;
                        } else if (c === '\xa0') {
                            mode = STATE.TAL;
                        } else if (c >= '0' && c <= '9') {
                            if (c !== '0' || svar.helAntal > 0) {
                                svar.helAntal++;
                            }
                            mode = STATE.TAL;
                        } else {
                            return svar;
                        }
                        break;
                    case STATE.DOT:
                        if (c >= '0' && c <= '9') {
                            svar.decAntal++;
                            mode = STATE.DOTTAL;
                            break;
                        } else {
                            return svar;
                        }
                    case STATE.DOTTAL:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                        } else if (c === 'e' || c === 'E') {
                            mode = STATE.EXPO;
                        } else if (c === '\xa0') {
                            mode = STATE.DOTTAL;
                        } else if (c >= '0' && c <= '9') {
                            svar.decAntal++;
                            mode = STATE.DOTTAL;
                        } else {
                            return svar;
                        }
                        break;
                    case STATE.EXPO:
                        if (c >= '0' && c <= '9') {
                            svar.exp = c.charCodeAt(0) - 48;
                            mode = STATE.EXPTAL;
                            break;
                        } else if (c === '-' || c === '+') {
                            expMinus = (c === '-');
                            mode = STATE.EXPSIGN;
                            break;
                        } else {
                            return svar;
                        }
                    case STATE.EXPSIGN:
                        if (c >= '0' && c <= '9') {
                            svar.exp = c.charCodeAt(0) - 48;
                            mode = STATE.EXPTAL;
                            break;
                        } else {
                            return svar;
                        }
                    case STATE.EXPTAL:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                            break;
                        } else if (c >= '0' && c <= '9') {
                            svar.exp = (svar.exp * 10) + c.charCodeAt(0) - 48;
                            break;
                        } else if (c === '\xa0') {
                            break;
                        } else {
                            return;
                        }
                    case STATE.SLUT:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            break;
                        } else {
                            return svar;
                        }
                    default:
                        throw "impossible";
                }
            }
            if (expMinus) {
                svar.exp = (-svar.exp);
            }
            svar.vsiffror = svar.helAntal + svar.decAntal;
            if (svar.vsiffror === 0) {
                svar.vsiffror = 1;
            }
            svar.valid = (mode === STATE.TAL ||
                mode === STATE.DOTTAL ||
                mode === STATE.EXPTAL ||
                mode === STATE.SLUT);
            return svar;
        };

        // Snygga till flyttal i strängform.
        // s - flyttal i strängform
        //	*ta bort thousandseparator
        // I fixed form med decimaler
        //   *ta bort avslutande nollor
        //	  *ta bort decimal separator om på sista positionen
        // I E-form
        //	  *ta bort nollor direkt till höger om Exponent
        //	  *ta bort nollor direkt till vänster om Exponent
        //	  *ta bort decimal separator om på sista positionen
        export function trimFloat(s:string ):string {
            // ta bort alla tusendelasavskilljare
            s = s.replace(/\xa0/g, "");
            let pExp = s.indexOf('E');
            if (pExp === -1) {
                pExp = s.indexOf('e');
            }
            if (pExp !== -1) {
                // ta bort nollor till vänster och höger om E
                while (s.length > pExp + 1 && s.charAt(pExp + 1) === '0') {
                    s = Text.remove(s, pExp + 1, 1);
                }
                while (pExp > 0 && s.charAt(pExp - 1) === '0') {
                    s = Text.remove(s, pExp - 1, 1);
                    pExp--;
                }
                // Om vi bara har "." eller "," kvar så ta bort den också.
                if (pExp > 0 && (s.charAt(pExp - 1) === '.' || s.charAt(pExp - 1) === ',')) {
                    s = Text.remove(s, pExp - 1, 1);
                    pExp--;
                }
            } else {
                // ta bort avslutande nollor
                let pDot = s.indexOf('.');
                if (pDot === -1) {
                    pDot = s.indexOf(',');
                }
                if (pDot !== -1) {
                    while (s.charAt(s.length - 1) === '0') {
                        s = Text.remove(s, s.length - 1, 1);
                    }
                    // Om vi bara har "." eller "," klet så ta bort den också.
                    if (s.length - 1 === pDot) {
                        s = Text.remove(s, pDot, 1);
                    }
                }
            }
            return s;
        };

        // Avrunda till värdesiffror
        export function roundToSignficant(v:number, digits:number):number {
            let a;
            if (v === 0.0) {
                return 0.0;
            } else {
                let exp = Math.floor((Math.log(Math.abs(v)) / Math.LN10)) - digits + 1;
                if (exp >= 0) {
                    a = Math.pow(10, exp);
                    return Math.round(v / a) * a;
                } else {
                    a = Math.pow(10, -exp);
                    return Math.round(v * a) / a;
                }
            }
        };
        export function floatToStrSignificant(v:number, digits:number, maxLeadingZeros:number):string {
            let bas = 0;
            let q;
            let b;
            let neg;
            let c;

            if (v === 0.0) {
                neg = false;
                q = digits - 1;
                b = 0;
            } else {
                neg = v < 0;
                v = Math.abs(v);
                let e = Math.floor(Math.log(v) / Math.LN10);
                let a = Math.pow(10, e - digits + 1);
                b = Math.round(v / a);
                // ibland blir det avrundningsfel som leder till
                // att e blivit ett steg för liten, t.ex.
                // om v=0,000001 digit=4   -> e=-7, b=10000
                if (b >= Math.pow(10, digits)) {
                    e++;
                }
                // 0 <= b < 10^n  där n är # värdesiffror.
                q = digits - e - 1;
                // antal decimaler (q) som skall visas i normalform är q=(vsiffror-e)
                // om q<0 så måste E form visas (V2, 1.0E3 för 100)
                // om q>=vsiffror+x så måste E form visas (V2, 1.0E-5 för 0.00001)
                if (q < 0 || q >= digits + maxLeadingZeros) {
                    bas = -q + (digits - 1);
                    q = digits - 1;
                }
            }

            let buf = [];
            let t = 0;
            if (bas !== 0) {
                let negBase = bas < 0;
                if (bas < 0) {
                    bas = -bas;
                }
                while (bas !== 0) {
                    c = bas % 10;
                    bas = (bas - c) / 10;
                    buf.push(String.fromCharCode(c + 48));
                }
                if (negBase) {
                    buf.push("-");
                }
                buf.push('E');
            }
            while (b !== 0 || t <= q) {
                c = b % 10;
                b = (b - c) / 10;
                if (t === q && t !== 0) {
                    buf.push('.');
                }
                buf.push(String.fromCharCode(c + 48));
                t++;
            }
            if (neg) {
                buf.push('-');
            }
            return buf.reverse().join("");
        };
        // Flyttals formatering. Hantering av värdesiffror m.m.
        // Sätt upplösning. V[värdesiffror] eller 0.1 (en decimal)
        //  Sätt till tomt för auto
        // Exempel:
        // t=12.34, floatRes="V3" -> Convert="12.3"
        // t=12.34, floatRes="V2" -> Convert="12"
        // t=12.34, floatRes="V1" -> Convert="10"
        // t=12.34, floatRes="10" -> Convert="10"
        // t=12.34, floatRes="1" -> Convert="12"
        // t=12.34, floatRes="0.1" -> Convert="12.3"
        // t=12.34, floatRes="0.25" -> Convert="12.25"

        export interface IFloatFormat {
            ftype: number;
            decimalChar: string;
            decimals: number;
            fround: number;
            usesRound: boolean;
            vsiffror: number;
            floatRes: string;
            round: (n:number) => number;
            convert: (n:number) => string;
            moreAccurate(f:IFloatFormat): boolean;
        }

        export function floatFormat(aFloatRes: string, aDecimalChar?: string): IFloatFormat {
            let obj = {
                ftype: 0,     // Auto=0, Significant=1, Fixed=2
                decimalChar: aDecimalChar || ".",
                decimals: 0,
                fround: 0,
                usesRound: false,
                vsiffror: 0,
                floatRes: $.trim(aFloatRes),

                // Avrunda enligt angiven floatRes
                // returnerar v avrundat enligt floatres. Om Auto så returneras v oförändrad
                round: function (v:number) { return v; },
                // Omvandla flyttal till sträng enligt floatRes
                // convert(v)
                convert: <(n:number) => string>null,

                // Testa om this är mer nogrann än ff.
                // fixed betraktas alltid som mer nogrann än significant medan auto är minst nogrann.
                moreAccurate: function (ff:IFloatFormat) {
                    let x = this.ftype - ff.ftype;
                    if (x > 0) {
                        return true;
                    } else if (x < 0) {
                        return false;
                    } else {
                        switch (this.ftype) {
                            case 0: // Auto:
                                return true;
                            case 1: // Significant:
                                return this.vsiffror >= ff.vsiffror;
                            case 2: // Fixed:
                                return this.fround <= ff.fround;
                            default:
                                throw "Unkown FloatFormatType";
                        }
                    }
                }
            };

            if (obj.floatRes.length === 0) {
                obj.ftype = 0; // Auto;
                obj.usesRound = false;
                obj.convert = function (v:number) {
                    v = this.round(v);
                    let s = FloatEx.trimFloat(v.toString());
                    if (this.decimalChar !== ".") {
                        s = s.replace(".", this.decimalChar);
                    }
                    return s;
                };
            } else {
                if (obj.floatRes.charAt(0) === 'v' || obj.floatRes.charAt(0) === 'V') {
                    obj.ftype = 1; // Significant;
                    obj.usesRound = true;
                    obj.vsiffror = parseInt(obj.floatRes.substr(1), 10);
                    if (obj.vsiffror > 9 || obj.vsiffror < 1) {
                        obj.vsiffror = 9;
                    }
                    obj.round = function (v) { return FloatEx.roundToSignficant(v, this.vsiffror); };
                    obj.convert = function (v:number) {
                        let s = FloatEx.floatToStrSignificant(v, this.vsiffror, 4);
                        if (this.decimalChar !== ".") {
                            s = s.replace(".", this.decimalChar);
                        }
                        return s;
                    };
                } else {
                    obj.ftype = 2;    // Fixed;
                    let b = FloatEx.breakStrFloat(obj.floatRes);
                    if (!b.valid) {
                        throw "Invalid floatRes (" + obj.floatRes + ")";
                    }
                    obj.decimals = b.decAntal;
                    obj.fround = parseFloat(obj.floatRes.replace(/,/g, "."));
                    if (this.fround !== 0.0) {
                        obj.usesRound = true;
                        obj.round = function (v) { return Math.round(v / this.fround) * this.fround; };
                    }
                    obj.convert = function (v:number) {
                        let s;
                        v = this.round(v);
                        if (this.decimals >= 0) {
                            s = v.toFixed(this.decimals);
                        } else {
                            s = FloatEx.trimFloat(v.toString());
                        }
                        if (this.decimalChar !== ".") {
                            s = s.replace(".", this.decimalChar);
                        }
                        return s;
                    };
                }
            }
            return obj;
        }
    };

    export namespace JSONex {
        let reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/;
        let reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

        export function parseWithDate(json:any) {
            /// <summary>
            /// parses a JSON string and turns ISO or MSAJAX date strings
            /// into native JS date objects
            /// </summary>
            /// <param name="json" type="var">json with dates to parse</param>
            /// </param>
            /// <returns type="value, array or object" />
            try {
                let res = JSON.parse(json,
                    function (key, value) {
                        if (typeof value === 'string') {
                            let a = reISO.exec(value);
                            if (a) {
                                return new Date(Date.UTC(+ a[1], + a[2] - 1,
                                    + a[3], + a[4], + a[5], + a[6]));
                            }
                            a = reMsAjax.exec(value);
                            if (a) {
                                let b = a[1].split(/[-+,.]/);
                                return new Date(b[0] ? (+ b[0]) : (0 - (+ b[1])));
                            }
                        }
                        return value;
                    });
                return res;
            } catch (e) {
                // orignal error thrown has no error message so rethrow with message
                throw new Error("JSON content could not be parsed");
            }
        };
        export function dateStringToDate(dtString:string) {
            /// <summary>
            /// Converts a JSON ISO or MSAJAX string into a date object
            /// </summary>
            /// <param name="" type="var">Date String</param>
            /// <returns type="date or null if invalid" />
            let a = reISO.exec(dtString);
            if (a) {
                return new Date(Date.UTC(+ a[1], + a[2] - 1, + a[3],
                    + a[4], + a[5], + a[6]));
            }
            a = reMsAjax.exec(dtString);
            if (a) {
                let b = a[1].split(/[-,.]/);
                return new Date(+ b[0]);
            }
            return null;
        };
        function stringifyWcf(json:any) {
            /// <summary>
            /// Wcf specific stringify that encodes dates in the
            /// a WCF compatible format ("/Date(9991231231)/")
            /// Note: this format works ONLY with WCF.
            ///       ASMX can use ISO dates as of .NET 3.5 SP1
            /// </summary>
            /// <param name="key" type="var">property name</param>
            /// <param name="value" type="var">value of the property</param>
            return JSON.stringify(json, function (key, value) {
                if (typeof value === "string") {
                    let a = reISO.exec(value);
                    if (a) {
                        let val = '/Date(' +
                            new Date(Date.UTC(+ a[1], + a[2] - 1,
                                + a[3], + a[4],
                                + a[5], + a[6])).getTime() + ')/';
                        this[key] = val;
                        return val;
                    }
                }
                return value;
            });
        };
    }

    export namespace TimeSpanEx {
        let _timespanEnheter= [
            { txt: "", faktor: 1, aprox: 0 },
            { txt: "ms", faktor: 1, aprox: 0 },
            { txt: "s", faktor: 1000, aprox: 0 },
            { txt: "m", faktor: 60 * 1000, aprox: 90 * 1000 },
            { txt: "t", faktor: 60 * 60 * 1000, aprox: 90 * 60 * 1000 },
            { txt: "h", faktor: 60 * 60 * 1000, aprox: 90 * 60 * 1000 },
            { txt: "d", faktor: 24 * 60 * 60 * 1000, aprox: 48 * 60 * 60 * 1000 }
        ];

        export function msek2timeLenStr(msek:number, addWhiteSpace?:boolean) {
            let s = "";
            let t;
            if (msek === 0) {
                return "0ms";
            }
            for (t = _timespanEnheter.length - 1; t >= 0; t--) {
                let e = _timespanEnheter[t];
                let fak = e.faktor;
                if (msek >= fak) {
                    let n = Math.floor(msek / fak);
                    msek = Math.round(msek - (n * fak));
                    s += n.toString() + e.txt;
                    if (msek !== 0 && addWhiteSpace) {
                        s += " ";
                    }
                }
            }
            return s;
        };

        export function msek2approxTimeLenStr(msek:number, addWhiteSpace?:boolean) {
            let t;
            for (t = _timespanEnheter.length - 1; t >= 0; t--) {
                let e = _timespanEnheter[t];
                let fak = e.faktor;
                let aprox = e.aprox;
                if (msek >= aprox) {
                    let n = Math.floor((msek + fak / 2) / fak) * fak;
                    if (n === 0) {
                        return addWhiteSpace ? "0 s" : "0s";
                    } else {
                        return msek2timeLenStr(n, addWhiteSpace);
                    }
                }
            }
            return "";
        };

        export function timeLenStr2msek(s:string) {
            let state = 0;
            let acc = 0;
            let enhet = "";
            let isDigit = /[0-9]/;
            let isSpace = /[ \t]/;
            let msekSum = 0;
            let c;
            let n;
            let t;
            for (n = 0; n <= s.length; n++) {
                if (n === s.length) {
                    c = "";
                } else {
                    c = s.charAt(n);
                }
                statetest:
                switch (state) {
                    case 0:
                        // skippa whitespace
                        // 0-9 till nästa
                        // allt annat ger fel
                        if (isDigit.test(c)) {
                            acc = 0;
                            enhet = "";
                            state = 1;
                            n--;
                        } else if (!isSpace.test(c) && c !== "") {
                            throw "Invalid timeLenStr, left-crap: " + s;
                        }
                        break;
                    case 1:
                        // 0-9
                        // övrigt till nästa
                        if (isDigit.test(c)) {
                            acc = acc * 10 + parseInt(c, 10);
                        } else {
                            n--;
                            state = 2;
                        }
                        break;
                    case 2:
                        // skippa whitespace
                        if (!isSpace.test(c)) {
                            n--;
                            state = 3;
                        }
                        break;
                    case 3:
                        // space, tomt, siffror till 4
                        // övrigt till enhet
                        if (isSpace.test(c) || isDigit.test(c) || c === "") {
                            n--;
                            state = 4;
                        } else {
                            enhet += c;
                        }
                        break;
                    case 4:
                        enhet = enhet.toLowerCase();
                        for (t = 0; t < _timespanEnheter.length; t++) {
                            let i = _timespanEnheter[t];
                            if (i.txt === enhet) {
                                msekSum += acc * i.faktor;
                                state = 0;
                                n--;
                                break statetest;
                            }
                        }
                        throw "Invalid timeLenStr unit: " + s;
                    default:
                        throw "impossible";
                }
            }
            return msekSum;
        }
    };

    export function HumanReadableFileSize(n:number) {
        if (n < 10000) {
            return n.toString();
        } else if (n < 10000 * 1024) {
            return Math.round(n / 1024).toString() + " Kb";
        } else if (n < 10000 * 1024 * 1024) {
            return Math.round(n / (1024 * 1024)).toString() + " Mb";
        } else if (n < 10000 * 1024 * 1024 * 1024) {
            return Math.round(n / (1024 * 1024 * 1024)).toString() + " Gb";
        } else {
            return Math.round(n / (1024 * 1024 * 1024 * 1024)).toString() + " Tb";
        }
    };

    export namespace DateTimeEx {

        export function isSameDate (d1:Date,d2:Date) {
            return (
                d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth() &&
                d1.getDate() === d2.getDate()
            );
        }

        export function addDays(date:Date, days:number):Date {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }

        export function justDate(d:Date):Date {
            let year = d.getFullYear();
            let month = d.getMonth();
            let day = d.getDate();
            return new Date(year,month,day);            
        }

        var veckodagar=['sön','mån','tis',"ons","tor","fre","lör"];
        var monader=['jan','feb',"mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"];

        export function HumanReadableTime(tiddt:Date):string
        {
            let nudt=new Date();
            let delta = nudt.getTime() - tiddt.getTime();
            if (delta < 60* 60 * 1000) {
                let minuter=Math.floor(delta / (60*1000));
                return `${minuter} minuter sedan`;
            } else if (isSameDate(nudt,tiddt)) {
                return `idag ${hhmm100ToString(tiddt.getHours()*100+tiddt.getMinutes() )}`;
            } else if (isSameDate(addDays(nudt,-1),tiddt)) {
                return `igår ${hhmm100ToString(tiddt.getHours()*100+tiddt.getMinutes() )}`;
            } else if (addDays( justDate(nudt), -5).getTime() < justDate(tiddt).getTime() ) {
                return `${veckodagar[tiddt.getDay()]} ${hhmm100ToString(tiddt.getHours()*100+tiddt.getMinutes() )}`;
            } else if (addDays( justDate(nudt), -290).getTime() < justDate(tiddt).getTime() ) {
                return `${tiddt.getDate()} ${monader[tiddt.getMonth()]} ${hhmm100ToString(tiddt.getHours()*100+tiddt.getMinutes() )}`;
            } else {
                return dateToString(tiddt)+" "+hhmm100ToString(tiddt.getHours()*100+tiddt.getMinutes() );
            }
        }

        export function UTC2local(tid:Date) {
            let x = new Date(tid.getUTCFullYear(), tid.getUTCMonth(), tid.getUTCDate(),
                tid.getUTCHours(), tid.getUTCMinutes(), tid.getUTCSeconds());
            x.setTime(x.getTime() + daylightSaving(tid) + 3600000);
            return x;
        };
        export function local2UTC(tid: Date) {
            let x = new Date(tid.getUTCFullYear(), tid.getUTCMonth(), tid.getUTCDate(),
                tid.getUTCHours(), tid.getUTCMinutes(), tid.getUTCSeconds());
            x.setTime(x.getTime() - daylightSaving(tid) - 3600000);
            return x;
        };

        export function daylightSaving(tid: Date) {
            return (tid.getTime() > _findDay(tid, 0, 3-1, 2, -1).getTime())
                && (tid.getTime() < _findDay(tid, 0, 10-1, 2, -1).getTime())
                ? 3600000 : 0;
        };

        function _findDay(tid:Date, d:number, m:number, h:number, p:number) {
            let week = (p < 0) ? 7 * (p + 1) : 7 * (p - 1);
            let nm = (p < 0) ? m + 1 : m;
            let x = new Date(tid.getUTCFullYear(), nm, 1, h, 0, 0);
            let dOff = 0;
            if (p < 0) {
                x.setTime(x.getTime() - 86400000);
            }
            if (x.getDay() !== d) {
                dOff = (x.getDay() < d) ? (d - x.getDay()) : -(x.getDay() - d);
                if (p < 0 && dOff > 0) {
                    week -= 7;
                }
                if (p > 0 && dOff < 0) {
                    week += 7;
                }
                x.setTime(x.getTime() + ((dOff + week) * 86400000));
            }
            return x;
        };

        export function dateToString(d:Date):string {			// datumdelean av date i localtime omvandlas till sträng YYYY-MM-DD
            let y = (d.getFullYear()).toString();
            let m = (d.getMonth() + 1).toString();
            let d2 = d.getDate().toString();
            if (m.length === 1) {
                m = "0" + m;
            }
            if (d2.length === 1) {
                d2 = "0" + d2;
            }
            return y + "-" + m + "-" + d2;
        };
        export function timeToString(d:Date,inludeSek:boolean=true):string {			// tiddelean av date i localtime omvandlas till sträng HH:MM (:SS)
            let h = (d.getHours()).toString();
            let m = (d.getMinutes()).toString();
            if (h.length === 1) {
                h = "0" + h;
            }
            if (m.length === 1) {
                m = "0" + m;
            }
            if(inludeSek) {
                let s = d.getSeconds().toString();
                if (s.length === 1) {
                    s = "0" + s;
                }
                return h + ":" + m + ":" + s;
            } else 
                return h + ":" + m;
        };

        export function hhmm100ToDate(hhmm100:number):Date {
            if (hhmm100 === null) {
                return null;
            } else {
                let h = Math.floor(hhmm100 / 100);
                let m = hhmm100 % 100;
                return new Date(0, 0, 0, h, m, 0);
            }
        };
        export function dateTohhmm100(adate:Date):number {
            if (adate === null) {
                return null;
            } else {
                return adate.getHours() * 100 + adate.getMinutes();
            }
        };

        export function hhmm100_to_antalMin(hhmm100:number):number {
            let h = Math.floor(hhmm100 / 100);
            let m = hhmm100 % 100;
            if (m > 59 || m < 0) {
                throw { name: "InvalidData", message: "invalid hhmm100, " + hhmm100 };
            }
            return h * 60 + m;
        };

        export function antalMin_to_hhmm100(hhmm100:number):number {
            let h = Math.floor(hhmm100 / 60);
            let m = hhmm100 % 60;
            return h * 100 + m;
        };

        export function parsehhmm100(s:string, allow2400?:boolean, throwIfOutOfRange?:boolean) {
            let m = 0;
            let h = 0;
            let st = 0;
            let antal = 0;
            let t = 0;

            let antaldigits = 0;    // i rad utan separator
            let lastWasDig = false;
            let thisIsDig = false;
            let q;
            for (q = 0; q < s.length; q++) {
                thisIsDig = (s[q] >= "0" && s[q] <= "9");
                if (thisIsDig && !lastWasDig) {
                    antaldigits = 1;
                }
                if (thisIsDig && lastWasDig) {
                    antaldigits++;
                }
                lastWasDig = thisIsDig;
            }

            while (t < s.length) {
                let ischar = s[t] >= "0" && s[t] <= "9";
                let n = ischar ? parseInt(s[t], 10) : 0;
                let issep = s[t] === ':';
                let iswhite = s[t] === ' ';
                if (st === 0) {
                    if (iswhite) {
                        t++;
                    } else {
                        st = 1;
                    }
                } else if (st === 1) {
                    if (ischar) {
                        h = h * 10 + n;
                        antal++;
                        if (antaldigits > 2 && antal >= antaldigits - 2) {
                            st = 2;
                        }
                        t++;
                    } else if (issep || iswhite) {
                        st = 2;
                    } else {
                        throw "Syntax error";
                    }
                } else if (st === 2) {
                    if (issep || iswhite) {
                        t++;
                    } else {
                        st = 3;
                    }
                } else if (st === 3) {
                    if (ischar) {
                        m = m * 10 + n;
                        t++;
                    } else if (iswhite) {
                        st = 4;
                    } else {
                        throw "Syntax error";
                    }
                } else if (st === 4) {
                    if (iswhite) {
                        t++;
                    } else {
                        throw "Syntax error";
                    }
                }
            }
            let svar = ((h + Math.floor(m / 60)) * 100) + (m % 60);
            if (allow2400 === undefined) {
                allow2400 = true;
            }
            if (svar > (allow2400 ? 2400 : 2359)) {
                if (throwIfOutOfRange === undefined || throwIfOutOfRange) {
                    throw "Syntax error";
                }
            }
            return svar;
        };

        export function hhmm100ToString(hhmm100:number):string {
            let h = Math.floor(hhmm100 / 100);
            let m = Math.round(hhmm100 - (h * 100));
            return (h <= 9 ? "0" : "") + h.toString() + ":" + (m <= 9 ? "0" : "") + m.toString();
        };

        export function dayOfYear(d:Date):number {
            let onejan = new Date(d.getFullYear(), 0, 1);
            return Math.ceil((d.getTime() - onejan.getTime()) / 86400000);
        };

        // Lite osäker på om denna är korrekt.
        // Här förutsätts att 2005-05-19T15:01:42.0000000+02:00
        //                    -------- UTC --------------
        // men så är det inte?
        export function decodeXSDdateTime(s:string):string {
            if (s === undefined) {
                return undefined;
            }
            if (s === null) {
                return null;
            }
            let x = new Date();
            // 2005-05-19T15:01:42.0000000+02:00
            // 0123456789012345678
            x.setUTCFullYear(parseInt( s.substring(0, 4),10), parseInt(s.substring(5, 7),10) - 1, parseInt(s.substring(8, 10),10));
            x.setUTCHours(parseInt(s.substring(11, 13),10), parseInt(s.substring(14, 16),10), parseInt(s.substring(17, 19),10));

            // Eventuellt så skall daylightSaving() anropas med UTC tiden?

            // Innan 2007-05-29 så var raden nedan med, den gav oss dock fel tid när sommartid.
            // x.setTime(x.getTime()+daylightSaving(x));

            // x.setFullYear(s.substring(0,4),s.substring(5,7),s.substring(8,10));
            // x.setHours(s.substring(11,13),s.substring(14,16),s.substring(17,19));
            let mo = (x.getMonth() + 1).toString();
            let da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }
            let hr = x.getHours().toString();
            if (hr.length === 1) {
                hr = "0" + hr;
            }
            let mn = x.getMinutes().toString();
            if (mn.length === 1) {
                mn = "0" + mn;
            }
            let se = x.getSeconds().toString();
            if (se.length === 1) {
                se = "0" + se;
            }

            return x.getFullYear() + "-" + mo + "-" + da + " " + hr + ":" + mn + ":" + se;
        };

        //Baserad på: DepressedPress.com, DP_DateExtensions
        //Author: Jim Davis, the Depressed Press of Boston
        //Created: June 20, 2006
        //Contact: webmaster@depressedpress.com
        //Website: www.depressedpress.com
        //http://depressedpress.com/javascript-extensions/
        export function parseIso8601(CurDate:string):Date {
            // Check the input parameters
            if (typeof CurDate !== "string" || CurDate === "") {
                return null;
            }

            ///* jshint ignore:start

            // Set the fragment expressions
            let S = "[\\-/:.]";
            let Yr = "((?:1[6-9]|[2-9][0-9])[0-9]{2})";
            let Mo = S + "((?:1[012])|(?:0[1-9])|[1-9])";
            let Dy = S + "((?:3[01])|(?:[12][0-9])|(?:0[1-9])|[1-9])";
            let Hr = "(2[0-4]|[01]?[0-9])";
            let Mn = S + "([0-5]?[0-9])";
            let Sd = "(?:" + S + "([0-5]?[0-9])(?:[.,]([0-9]+))?)?";
            let TZ = "(?:(Z)|(?:([\+\-])(1[012]|[0]?[0-9])(?::?([0-5]?[0-9]))?))?";
            // RegEx the input
            // First check: Just date parts (month and day are optional)
            // Second check: Full date plus time (seconds, milliseconds and TimeZone info are optional)
            let TF:any=new RegExp("^" + Yr + "(?:" + Mo + "(?:" + Dy + ")?)?" + "$").exec(CurDate);
            if (TF) {
                // do nothing
            } else {
                TF=TF = new RegExp("^" + Yr + Mo + Dy + "[Tt ]" + Hr + Mn + Sd + TZ + "$").exec(CurDate);
                if (TF) {
                    // do nothing
                }
            }
            // If the date couldn't be parsed, return null
            if (!TF) { return null; }
            // Default the Time Fragments if they're not present
            if (!TF[2]) { TF[2] = 1; } else { TF[2] = TF[2] - 1; }
            if (!TF[3]) { TF[3] = 1; }
            if (!TF[4]) { TF[4] = 0; }
            if (!TF[5]) { TF[5] = 0; }
            if (!TF[6]) { TF[6] = 0; }
            if (!TF[7]) { TF[7] = 0; }
            if (!TF[8]) { TF[8] = null; }
            if (TF[9] !== "-" && TF[9] !== "+") { TF[9] = null; }
            if (!TF[10]) { TF[10] = 0; } else { TF[10] = TF[9] + TF[10]; }
            if (!TF[11]) { TF[11] = 0; } else { TF[11] = TF[9] + TF[11]; }
            // If there's no timezone info the data is local time
            if (!TF[8] && !TF[9]) {
                return new Date(TF[1], TF[2], TF[3], TF[4], TF[5], TF[6], TF[7]);
            }
            // If the UTC indicator is set the date is UTC
            if (TF[8] === "Z") {
                return new Date(Date.UTC(TF[1], TF[2], TF[3], TF[4], TF[5], TF[6], TF[7]));
            }
            // If the date has a timezone offset
            if (TF[9] === "-" || TF[9] === "+") {
                // Get current Timezone information
                let CurTZ = new Date().getTimezoneOffset();

                // KEYLOGIC - ändrat strängoperation till multiplicering
                let CurTZh = TF[10] - ((CurTZ >= 0 ? -1 : 1) * (Math.floor(Math.abs(CurTZ) / 60)));
                let CurTZm = TF[11] - ((CurTZ >= 0 ? -1 : 1) * (Math.abs(CurTZ) % 60));

                // Return the date
                return new Date(TF[1], TF[2], TF[3], TF[4] - CurTZh, TF[5] - CurTZm, TF[6], TF[7]);
            }
            // If we've reached here we couldn't deal with the input, return null

            // jshint ignore:end

            return null;
        };

        // iso8601Format
        // Formats a date using an ISO8601-compliant format
        //Baserad på: DepressedPress.com, DP_DateExtensions
        //Author: Jim Davis, the Depressed Press of Boston
        //Created: June 20, 2006
        //Contact: webmaster@depressedpress.com
        //Website: www.depressedpress.com
        //http://depressedpress.com/javascript-extensions/
        export function iso8601Format(x:Date, includeTime?:boolean, isUTC?:boolean) {

            // Set the default
            if (includeTime === undefined) {
                includeTime = true;
            }

            let s;

            let mo = (x.getMonth() + 1).toString();
            let da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }

            s = x.getFullYear().toString() + "-" + mo + "-" + da;

            if (includeTime) {
                let hr = x.getHours().toString();
                if (hr.length === 1) {
                    hr = "0" + hr;
                }
                let mn = x.getMinutes().toString();
                if (mn.length === 1) {
                    mn = "0" + mn;
                }
                let se = x.getSeconds().toString();
                if (se.length === 1) {
                    se = "0" + se;
                }
                s = s + "T" + hr + ":" + mn + ":" + se;

                if (isUTC) {
                    s += "Z";
                } else {
                    // Get TimeZone Information
                    let TimeZoneOffset = x.getTimezoneOffset();
                    let TimeZoneInfo = (TimeZoneOffset >= 0 ? "-" : "+") + ("0" + (Math.floor(Math.abs(TimeZoneOffset) / 60))).slice(-2)
                            + ":" + ("00" + (Math.abs(TimeZoneOffset) % 60)).slice(-2);
                    s += TimeZoneInfo;
                }

            }
            return s;
        }

        export function dateTimeToString(x:Date) {
            let s;

            let mo = (x.getMonth() + 1).toString();
            let da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }

            s = x.getFullYear().toString() + "-" + mo + "-" + da;

            let hr = x.getHours().toString();
            if (hr.length === 1) {
                hr = "0" + hr;
            }
            let mn = x.getMinutes().toString();
            if (mn.length === 1) {
                mn = "0" + mn;
            }
            let se = x.getSeconds().toString();
            if (se.length === 1) {
                se = "0" + se;
            }
            s = s + " " + hr + ":" + mn + ":" + se;
            return s;
        }


    };

    export namespace RegExprEx {
        export function wildcardToRegex(wildcard:string):string {
            let s = "";
            let i;
            let lastc = "";
            for (i = 0; i < wildcard.length; i++) {
                let c = wildcard.charAt(i);
                switch (c) {
                    case '*':
                        if (lastc !== "*") {
                            s = s + ".*";
                        }
                        break;
                    case '?':
                        s = s + ".";
                        break;
                    // escape special regexp-characters
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '$':
                    case '^':
                    case '.':
                    case '{':
                    case '}':
                    case '|':
                    case '\\':
                        s = s + "\\" + c;
                        break;
                    default:
                        s = s + c;
                        break;
                }
                lastc = c;
            }
            return ("^" + s + "$");
        }
        export function makeExakt(str:string):string {
            let s = "";
            let i;
            for (i = 0; i < str.length; i++) {
                let c = str.charAt(i);
                switch (c) {
                    case '^':
                    case '$':
                    case '*':
                    case '?':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '$':
                    case '^':
                    case '.':
                    case '{':
                    case '}':
                    case '|':
                    case '\\':
                        // escape special regexp-characters
                        s = s + "\\" + c;
                        break;
                    default:
                        s = s + c;
                        break;
                }
            }
            return s;
        }
    };

    export namespace ScaleTicks {
        export interface IScaleTicksCalced {
            startN: number;
            minor: number;
            major: number;
            subSteps: number;
            offsetMajor: number;
            formatValue(v: number, extradecimaler?: number): string;
        }

        export interface IScaleTicks {
            setScale(slow: number, shigh: number):void;
            getScaleHigh(): number;
            getScaleLow(): number;
            setAproxMajorCount(ny: number):void;
            getAproxMajorCount(): number;
            getDrawData(): IScaleTicksCalced;
        }

        export function createScaleTicks() {
            let data = [
                { major: 0.1, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 1.0, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 0.2, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 0.5, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 0.2, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.4, substeps: 4, Threshold: 3, offset: 0 },
                { major: 0.4, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 0 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 1 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 3 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.25, substeps: 5, Threshold: 3, offset: 0 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 2 }
            ];
            let needCalc = true;
            let fShigh:number;
            let fSlow:number;
            let fAproxMajorCount:number;
            let fstartN:number;
            let fminor:number;
            let fmajor:number;
            let fsubsteps:number;
            let foffsetMajor:number;

            let floatRes;
            let decimaler:number;

            function calcAntalMajor(substeps:number, major:number, offset:number) {
                let minor = major / substeps;

                let N0 = ~ ~(fSlow / minor);
                let N1 = ~ ~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~ ~(N1 / substeps) - ~ ~((N0 + substeps - 1) / substeps) + 1);
            }

            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }

                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }

                let delta = fShigh - fSlow;

                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }

                function myLog10(n:number) {
                    return Math.log(n) / Math.LN10;
                }

                let optimal = delta / fAproxMajorCount;
                let expo = (Math.floor(myLog10(Math.abs(optimal)))) + 1;
                let pow = Math.pow(10, expo);
                optimal = optimal / pow;    // 0.1 <= optimal < 1

                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;

                let bestAntal = 1000;
                let bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        let antal = calcAntalMajor(p.substeps, p.major * pow, p.offset);
                        let score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });

                fmajor = fmajor * pow;
                fminor = fmajor / fsubsteps;
                fstartN = ~ ~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }

                floatRes = fmajor;

                expo = (Math.floor(myLog10(Math.abs(fmajor))));
                if (expo < 0) {
                    decimaler = -expo;
                } else {
                    decimaler = 0;
                }
            }

            let obj = {
                setScale: function (slow:number, shigh:number) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny:number) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    let svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v:number, extradecimaler:number) {
                            extradecimaler = extradecimaler || 0;
                            //return Math.round(v/floatRes)*floatRes;
                            return v.toFixed(decimaler + extradecimaler);
                        }
                    };
                    return svar;
                }
            };
            return obj;
        };
        export function createScaleTicks_hhmm() {
            let data = [
                { major: 1, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 2, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 5, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 10, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 15, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 20, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 30, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 60, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 120, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 180, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 240, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 360, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 720, substeps: 3, Threshold: 9e99, offset: 0 }
            ];
            let needCalc = true;
            let fShigh:number;
            let fSlow:number;
            let fAproxMajorCount:number;
            let fstartN:number;
            let fminor:number;
            let fmajor:number;
            let fsubsteps:number;
            let foffsetMajor:number;

            let floatRes;
            let decimaler;

            function calcAntalMajor(substeps:number, major:number, offset:number) {
                let minor = major / substeps;

                let N0 = ~ ~(fSlow / minor);
                let N1 = ~ ~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~ ~(N1 / substeps) - ~ ~((N0 + substeps - 1) / substeps) + 1);
            }

            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }

                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }

                let delta = fShigh - fSlow;

                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }

                function myLog10(n:number) {
                    return Math.log(n) / Math.LN10;
                }

                //let optimal = delta / fAproxMajorCount;

                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;

                let bestAntal = 1000;
                let bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        let antal = calcAntalMajor(p.substeps, p.major, p.offset);
                        let score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });

                fmajor = fmajor;
                fminor = fmajor / fsubsteps;
                fstartN = ~ ~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }

                floatRes = fmajor;
                decimaler = 0;
            }

            let obj = {
                setScale: function (slow:number, shigh:number) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny:number) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    let svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v:number, extradecimaler:number) {
                            extradecimaler = extradecimaler || 0;
                            //return Math.round(v/floatRes)*floatRes;
                            return DateTimeEx.hhmm100ToString(
                                DateTimeEx.antalMin_to_hhmm100(Math.round(v))
                                );
                        }
                    };
                    return svar;
                }
            };
            return obj;
        };
        export function createScaleTicks_ms() {
            let needCalc = true;
            let fShigh:number;
            let fSlow:number;
            let fAproxMajorCount:number;

            let fstartN:number;
            let fminor:number;
            let fmajor:number;
            let fsubsteps:number;
            let foffsetMajor:number;

            function calcAntalMajor(substeps:number, major:number, offset:number) {
                let minor = major / substeps;

                let N0 = ~ ~(fSlow / minor);
                let N1 = ~ ~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~ ~(N1 / substeps) - ~ ~((N0 + substeps - 1) / substeps) + 1);
            }

            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }

                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }

                let delta = fShigh - fSlow;

                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }

                function myLog10(n:number) {
                    return Math.log(n) / Math.LN10;
                }

                let data = [
                    { major: 10 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 15 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 20 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 30 * 1000, substeps: 3, Threshold: 9e99, offset: 0 },
                    { major: 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },

                    { major: 2 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 60 * 1000, substeps: 3, Threshold: 9e99, offset: 0 },
                    { major: 4 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 5 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 10 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 15 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 20 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 30 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },

                    { major: 2 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 4 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 6 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 8 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },

                    { major: 12 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 24 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 2 * 24 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 24 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 4 * 24 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 5 * 24 * 60 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 7 * 24 * 60 * 60 * 1000, substeps: 7, Threshold: 9e99, offset: 0 }
                ];

                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;

                let bestAntal = 1000;
                let bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        let antal = calcAntalMajor(p.substeps, p.major, p.offset);
                        let score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });

                fmajor = fmajor;
                fminor = fmajor / fsubsteps;
                fstartN = ~ ~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }
            }

            let obj = {
                setScale: function (slow:number, shigh:number) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny:number) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    let svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v:number) {
                            if (v === 0) {
                                return "0";
                            } else {
                                return TimeSpanEx.msek2timeLenStr(v, false);
                            }
                        }
                    };
                    return svar;
                }
            };
            return obj;
        }
    };

    export namespace Coord {
        export interface IRectangleF {
            x: number;
            y: number;
            w: number;
            h: number;
        }

        export class RectangleF implements IRectangleF {
            public x: number;
            public y: number;
            public w: number;
            public h: number;
            constructor(r: IRectangleF) {
                this.x = r.x;
                this.y = r.y;
                this.w = r.w;
                this.h = r.h;
            }
            public right(): number {
                return this.x + this.w;
            }
            public bottom(): number {
                return this.y + this.h;
            }
            public centerY(): number {
                return this.y + this.h / 2;
            }
            public centerX(): number {
                return this.x + this.w / 2;
            }

            public contains(p: PointF) {
                return p.x >= this.x && p.y >= this.y && p.x < this.right() && p.y < this.bottom();
            }

            public union(r: IRectangleF): RectangleF {
                if (r) {
                    let left = Math.min(this.x, r.x);
                    let top = Math.min(this.y, r.y);
                    let right = Math.max(this.x + this.w, r.x + r.w);
                    let bottom = Math.max(this.y + this.h, r.y + r.h);
                    return new RectangleF({
                        x: left,
                        y: top,
                        w: right - left,
                        h: bottom - top
                    });
                } else {
                    return new RectangleF(this);
                }
            }
        }

        export interface SizeF {
            w: number;
            h: number;
        }

        export interface PointF {
            x: number;
            y: number;
        }
    }

    export namespace TEST {
        function floatex_floatToStrSignificant() {
            let ok = true;
            function test(v:number, digits:number, maxLeadingZeros:number, svar:string) {
                let r = FloatEx.floatToStrSignificant(v, digits, maxLeadingZeros);
                if (r !== svar) {
                    console.log("floatToStrSignificant(" + v + "," + digits + "," + maxLeadingZeros +
                            ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(1.234567, 4, 2, "1.235");
            test(1.234567, 3, 2, "1.23");
            test(1, 3, 2, "1.00");
            test(0, 3, 2, "0.00");
            test(56789, 3, 2, "5.68E4");
            test(-56789, 3, 2, "-5.68E4");
            test(0.012345, 2, 2, "0.012");
            test(0.0012345, 2, 2, "1.2E-3");
            test(0.00012345, 2, 2, "1.2E-4");
            return ok;
        };
        function floatex_roundToSignficant() {
            let ok = true;
            function test(v:number, digits:number, svar:number) {
                let r = FloatEx.roundToSignficant(v, digits);
                if (r !== svar) {
                    console.log("roundToSignficant(" + v + "," + digits + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(0.1234, 2, 0.12);
            test(-0.1234, 3, -0.123);
            test(0.123456, 4, 0.1235);
            test(56.789, 4, 56.79);
            test(0, 10, 0);
            test(12345, 2, 12000);
            test(-12345, 2, -12000);
            return ok;
        };
        function floatex_trimFloat() {
            let ok = true;
            function test(s:string, svar:string) {
                let r = FloatEx.trimFloat(s);
                if (r !== svar) {
                    console.log("trimFloat(" + s + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test("0.100", "0.1");
            test("1.00E009", "1E9");
            test("0", "0");
            test("1.2", "1.2");
            test("1.0", "1");
            test("1\xa0000.0", "1000");
            return ok;
        };
        function floatex_floatFormat() {
            let ok = true;
            function test(s:string, v:number, svar:string, c:string) {
                let ff = FloatEx.floatFormat(s, c);
                let r = ff.convert(v);
                if (r !== svar) {
                    console.log("floatFormat(" + s + ").convert(" + v + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test("1.00", 2, "2.00", ".");
            test("1.00", 2, "2,00", ",");
            test("V3", 12345, "1.23E4", ".");
            test("V2", 1.2345, "1.2", ".");
            test("0.25", 4.567, "4.50", ".");
            return ok;
        };
        function DateTimeEx_hhmm100_to_antalMin() {
            let ok = true;
            function test(n:number, svar:number) {
                let r = DateTimeEx.hhmm100_to_antalMin(n);
                if (r !== svar) {
                    console.log("hhmm100_to_antalMin(" + r + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(900, 9 * 60);
            test(1730, 17 * 60 + 30);
            return ok;
        };
        function DateTimeEx_antalMin_to_hhmm100() {
            let ok = true;
            function test(n:number, svar:number) {
                let r = DateTimeEx.antalMin_to_hhmm100(n);
                if (r !== svar) {
                    console.log("antalMin_to_hhmm100(" + r + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(9 * 60, 900);
            test(17 * 60 + 30, 1730);
        };

        function testTextSort() {
            let ok = true;
            let test = (s: string, n: number, l: number) => {
                let x = Text.findNumeric(s);
                if ((x.index !== n) || (x.len !== l)) {
                    console.log("Text.findNumerix('" + s + "') gav " + JSON.stringify(x) + ", förväntade "
                        + JSON.stringify({ index: n, len: l }));
                    ok = false;
                }
            };

            test('123', 0, 3);
            test('12abc123', 0, 2);
            test('abc123', 3, 3);
            test('abc123x', 3,3);
            test('hejhej', 6, 0);
            test('', -1, 0);

            let testC = (s1: string, s2: string, svar: number) => {
                let x = Text.compareStringWithNumeric(s1, s2);
                x = x < 0 ? -1 : (x > 0 ? 1 : 0);
                if (x !== svar) {
                    console.log("Text.compareStringWithNumeric('" + s1 + "','"+s2+"') gav "+x+", borde vara "+svar);
                    ok = false;
                }
            };

            testC('test', 'allo', 1);
            testC('allo', 'test', -1);
            testC('test', 'test', 0);

            testC('123', '015', 1);
            testC('015', '123', -1);
            testC('015', '015', 0);

            testC('123', '15', 1);
            testC('15', '123', -1);
            testC('15', '015', 0);

            testC('hej8', 'hej9', -1);
            testC('hej9', 'hej10', -1);

            testC('hej8allå', 'hej9allå', -1);
            testC('hej9allå', 'hej10allå', -1);
            testC('hej8allåA', 'hej8allåB', -1);
            testC('hej008allåA', 'hej8allåB', -1);
            testC('hej8allå', 'hej8allåB', -1);
            testC('8allå', '8allåB', -1);

            testC('hek8', 'hej9', 1);
            testC('hej8hej8', 'hej8hek8', -1);


            return ok;

        }

        export function testAll() {
            return testTextSort() &&
                floatex_floatToStrSignificant() &&
                floatex_roundToSignficant() &&
                floatex_floatFormat() &&
                floatex_trimFloat() &&
                DateTimeEx_hhmm100_to_antalMin() &&
                DateTimeEx_antalMin_to_hhmm100();
        }
    }
}
