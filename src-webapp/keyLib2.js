var KeyLib;
(function (KeyLib) {
    var MathEx;
    (function (MathEx) {
        function divInt(a, b) {
            return (a - a % b) / b;
        }
        MathEx.divInt = divInt;
    })(MathEx = KeyLib.MathEx || (KeyLib.MathEx = {}));
    var web;
    (function (web) {
        function getUrlParams() {
            var vars = {};
            var s = decodeURIComponent(window.location.href.replace(/\+/g, '%20'));
            var n = s.indexOf('?');
            if (n >= 0) {
                var hashes = s.slice(n + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    var hash = hashes[i].split('=');
                    if (hash.length === 1) {
                        vars[hash[0]] = null;
                    }
                    else {
                        if (hash.length > 1) {
                            vars[hash[0]] = hash[1];
                        }
                    }
                }
            }
            return vars;
        }
        web.getUrlParams = getUrlParams;
    })(web = KeyLib.web || (KeyLib.web = {}));
    var Base64;
    (function (Base64) {
        var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        function decode(input) {
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            var bytes = Math.round((input.length / 4) * 3);
            if (input.charAt(input.length - 1) === "=")
                bytes--;
            if (input.charAt(input.length - 2) === "=")
                bytes--;
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            var j = 0;
            var ab = new ArrayBuffer(bytes);
            var uarray = new Uint8Array(ab);
            for (i = 0; i < bytes; i += 3) {
                enc1 = _keyStr.indexOf(input.charAt(j++));
                enc2 = _keyStr.indexOf(input.charAt(j++));
                enc3 = _keyStr.indexOf(input.charAt(j++));
                enc4 = _keyStr.indexOf(input.charAt(j++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                uarray[i] = chr1;
                if (enc3 !== 64)
                    uarray[i + 1] = chr2;
                if (enc4 !== 64)
                    uarray[i + 2] = chr3;
            }
            return ab;
        }
        Base64.decode = decode;
    })(Base64 = KeyLib.Base64 || (KeyLib.Base64 = {}));
    var FloatEx;
    (function (FloatEx) {
        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
        FloatEx.isNumber = isNumber;
        function parseFloatEx(txt) {
            if (txt === null || txt === undefined || isNaN(txt)) {
                return txt;
            }
            return parseFloat(txt.replace(",", "."));
        }
        FloatEx.parseFloatEx = parseFloatEx;
        ;
    })(FloatEx = KeyLib.FloatEx || (KeyLib.FloatEx = {}));
    var Text;
    (function (Text) {
        function leftpad(s, size, c) {
            var r = s + "";
            if (!c)
                c = "0";
            while (r.length < size)
                r = c + r;
            return r;
        }
        Text.leftpad = leftpad;
        function templateString(text, obj, lev) {
            if (!text)
                return text;
            if (!lev)
                lev = 1;
            if (lev > 4)
                return text;
            var state = 0;
            var mark = 0;
            var n = 0;
            var svar = [];
            var len = text.length;
            while (n <= len) {
                switch (state) {
                    case 0:
                        if (n === len || text.charAt(n) === "{") {
                            svar.push(text.substring(mark, n));
                            mark = n + 1;
                            state = 1;
                        }
                        break;
                    case 1:
                        if (text.charAt(n) === "}") {
                            var solve = text.substring(mark, n);
                            var ny = undefined;
                            if (typeof obj === "function") {
                                ny = obj(solve);
                            }
                            else {
                                ny = (obj[solve]).toString();
                            }
                            if (ny)
                                svar.push(templateString(ny, obj, lev + 1));
                            mark = n + 1;
                            state = 0;
                        }
                        break;
                    default:
                        throw "impossible";
                }
                n++;
            }
            return svar.join("");
        }
        Text.templateString = templateString;
        function compareStringWithNumeric(a, b) {
            var aa = findNumeric(a);
            var bb = findNumeric(b);
            if (aa.index !== bb.index || aa.index === -1 || bb.index === -1) {
                return a.localeCompare(b);
            }
            else {
                if (aa.index === 0) {
                    var an = parseInt(a.substr(aa.index, aa.len), 10);
                    var bn = parseInt(b.substr(bb.index, bb.len), 10);
                    var svar = an - bn;
                    if (svar !== 0)
                        return svar;
                    return compareStringWithNumeric(a.substr(aa.index + aa.len), b.substr(bb.index + bb.len));
                }
                else {
                    var svar = a.substr(0, aa.index).localeCompare(b.substr(0, bb.index));
                    if (svar !== 0)
                        return svar;
                    return compareStringWithNumeric(a.substr(aa.index), b.substr(bb.index));
                }
            }
        }
        Text.compareStringWithNumeric = compareStringWithNumeric;
        function findNumeric(s) {
            if (!s) {
                return { index: -1, len: 0 };
            }
            var t = 1;
            if (s.charAt(0) >= "0" && s.charAt(0) <= "9") {
                while (t < s.length && (s.charAt(t) >= '0') && (s.charAt(t) <= '9')) {
                    t++;
                }
                return { index: 0, len: t };
            }
            else {
                while (t < s.length && !((s.charAt(t) >= '0') && (s.charAt(t) <= '9'))) {
                    t++;
                }
                if (t === s.length) {
                    return { index: t, len: 0 };
                }
                else {
                    var i = t;
                    while (t < s.length && (s.charAt(t) >= '0') && (s.charAt(t) <= '9')) {
                        t++;
                    }
                    return { index: i, len: t - i };
                }
            }
        }
        Text.findNumeric = findNumeric;
        function parseKeyEnum(str) {
            var svar = [];
            var lastNr = -1;
            $.each(str.split(";"), function () {
                var n = this.indexOf("=");
                if (n !== -1) {
                    var tals = $.trim(this.substr(0, n));
                    var intRegex = /^[\-]?\d+$/;
                    if (intRegex.test(tals)) {
                        lastNr = parseInt(tals, 10);
                    }
                    else {
                        n = -1;
                    }
                }
                if (n === -1) {
                    lastNr++;
                    svar[lastNr] = this.toString();
                }
                else {
                    svar[lastNr] = $.trim(this.substr(n + 1)).toString();
                }
            });
            return svar;
        }
        Text.parseKeyEnum = parseKeyEnum;
        ;
        function dbgenEnum2KeyEnum(str) {
            if (str.indexOf(',') === -1 || str.indexOf(';') !== -1) {
                return str;
            }
            var ny = [];
            var ss = str.split(",");
            var atN = 0;
            var atNy = 0;
            for (var n = 0; n < ss.length; n++) {
                var s = ss[n];
                if (n === 0 && FloatEx.isNumber(s)) {
                    atN = parseInt(ss[n], 10);
                }
                else {
                    var i1 = s.indexOf("=");
                    if (i1 !== -1) {
                        var r = s.substr(0, i1);
                        if (FloatEx.isNumber(r)) {
                            atN = parseInt(r, 10);
                            s = s.substr(i1 + 1);
                        }
                    }
                    if (atNy === atN) {
                        ny.push(s);
                    }
                    else {
                        ny.push(atN + "=" + s);
                    }
                    atN = atN + 1;
                    atNy = atN;
                }
            }
            return ny.join(";");
        }
        Text.dbgenEnum2KeyEnum = dbgenEnum2KeyEnum;
        function strToIntEx(s) {
            if (s.length > 2) {
                if (s[0] === "$") {
                    return parseInt(s.substr(1), 16);
                }
                else if (s.substr(0, 2) === "0x") {
                    return parseInt(s.substr(2), 16);
                }
                else if (s[s.length - 1] === "b") {
                    return parseInt(s.substr(0, s.length - 2), 2);
                }
            }
            return parseInt(s, 10);
        }
        Text.strToIntEx = strToIntEx;
        ;
        function htmlEncode(s) {
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            else {
                return s;
            }
        }
        Text.htmlEncode = htmlEncode;
        ;
        function htmlEncodeWithBR(s) {
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\r/g, '<br/>');
            }
            else {
                return s;
            }
        }
        Text.htmlEncodeWithBR = htmlEncodeWithBR;
        ;
        function htmlAttributeEncode(s) {
            if (s) {
                return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, "&quot;").replace(/'/g, '&#34;');
            }
            else {
                return s;
            }
        }
        Text.htmlAttributeEncode = htmlAttributeEncode;
        ;
        function jsStringEncode(s) {
            if (s) {
                return s.replace(/\\/g, '\\\\');
            }
            else {
                return s;
            }
        }
        Text.jsStringEncode = jsStringEncode;
        ;
        function remove(s, indx, len) {
            var svar = '';
            if (indx > 0) {
                svar = s.substring(0, indx);
            }
            if (indx + len < s.length) {
                svar += s.substring(indx + len, s.length);
            }
            return svar;
        }
        Text.remove = remove;
        function startsWith(small, s) {
            return _.startsWith(s, small);
        }
        Text.startsWith = startsWith;
        ;
        function endsWith(small, s) {
            return _.endsWith(s, small);
        }
        Text.endsWith = endsWith;
        ;
        function contains(small, s) {
            return _.includes(s, small);
        }
        Text.contains = contains;
    })(Text = KeyLib.Text || (KeyLib.Text = {}));
    (function (FloatEx) {
        function breakStrFloat(s) {
            var svar = {
                vsiffror: 0,
                exp: 0,
                helAntal: 0,
                decAntal: 0,
                valid: false
            };
            var STATE = {
                INITWHITE: 0,
                TALSIGN: 1,
                TAL: 2,
                DOT: 3,
                DOTTAL: 4,
                EXPO: 5,
                EXPSIGN: 6,
                EXPTAL: 7,
                SLUT: 8
            };
            var mode = STATE.INITWHITE;
            var expMinus = false;
            var t;
            var c;
            for (t = 0; t < s.length; t++) {
                c = s.charAt(t);
                switch (mode) {
                    case STATE.INITWHITE:
                        if (c >= '0' && c <= '9') {
                            if (c !== '0') {
                                svar.helAntal = 1;
                            }
                            mode = STATE.TAL;
                        }
                        else if (c === '+' || c === '-') {
                            mode = STATE.TALSIGN;
                        }
                        else if (c !== '\t' && c !== ' ' && c !== '\r' && c !== '\n' && c !== '\xa0') {
                            return svar;
                        }
                        break;
                    case STATE.TALSIGN:
                        if (c >= '0' && c <= '9') {
                            if (c !== '0') {
                                svar.helAntal = 1;
                            }
                            mode = STATE.TAL;
                        }
                        else {
                            return svar;
                        }
                        break;
                    case STATE.TAL:
                        if (c === '.' || c === ',') {
                            mode = STATE.DOT;
                        }
                        else if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                        }
                        else if (c === 'e' || c === 'E') {
                            mode = STATE.EXPO;
                        }
                        else if (c === '\xa0') {
                            mode = STATE.TAL;
                        }
                        else if (c >= '0' && c <= '9') {
                            if (c !== '0' || svar.helAntal > 0) {
                                svar.helAntal++;
                            }
                            mode = STATE.TAL;
                        }
                        else {
                            return svar;
                        }
                        break;
                    case STATE.DOT:
                        if (c >= '0' && c <= '9') {
                            svar.decAntal++;
                            mode = STATE.DOTTAL;
                            break;
                        }
                        else {
                            return svar;
                        }
                    case STATE.DOTTAL:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                        }
                        else if (c === 'e' || c === 'E') {
                            mode = STATE.EXPO;
                        }
                        else if (c === '\xa0') {
                            mode = STATE.DOTTAL;
                        }
                        else if (c >= '0' && c <= '9') {
                            svar.decAntal++;
                            mode = STATE.DOTTAL;
                        }
                        else {
                            return svar;
                        }
                        break;
                    case STATE.EXPO:
                        if (c >= '0' && c <= '9') {
                            svar.exp = c.charCodeAt(0) - 48;
                            mode = STATE.EXPTAL;
                            break;
                        }
                        else if (c === '-' || c === '+') {
                            expMinus = (c === '-');
                            mode = STATE.EXPSIGN;
                            break;
                        }
                        else {
                            return svar;
                        }
                    case STATE.EXPSIGN:
                        if (c >= '0' && c <= '9') {
                            svar.exp = c.charCodeAt(0) - 48;
                            mode = STATE.EXPTAL;
                            break;
                        }
                        else {
                            return svar;
                        }
                    case STATE.EXPTAL:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            mode = STATE.SLUT;
                            break;
                        }
                        else if (c >= '0' && c <= '9') {
                            svar.exp = (svar.exp * 10) + c.charCodeAt(0) - 48;
                            break;
                        }
                        else if (c === '\xa0') {
                            break;
                        }
                        else {
                            return;
                        }
                    case STATE.SLUT:
                        if (c === '\t' || c === ' ' || c === '\r' || c === '\n') {
                            break;
                        }
                        else {
                            return svar;
                        }
                    default:
                        throw "impossible";
                }
            }
            if (expMinus) {
                svar.exp = (-svar.exp);
            }
            svar.vsiffror = svar.helAntal + svar.decAntal;
            if (svar.vsiffror === 0) {
                svar.vsiffror = 1;
            }
            svar.valid = (mode === STATE.TAL ||
                mode === STATE.DOTTAL ||
                mode === STATE.EXPTAL ||
                mode === STATE.SLUT);
            return svar;
        }
        FloatEx.breakStrFloat = breakStrFloat;
        ;
        function trimFloat(s) {
            s = s.replace(/\xa0/g, "");
            var pExp = s.indexOf('E');
            if (pExp === -1) {
                pExp = s.indexOf('e');
            }
            if (pExp !== -1) {
                while (s.length > pExp + 1 && s.charAt(pExp + 1) === '0') {
                    s = Text.remove(s, pExp + 1, 1);
                }
                while (pExp > 0 && s.charAt(pExp - 1) === '0') {
                    s = Text.remove(s, pExp - 1, 1);
                    pExp--;
                }
                if (pExp > 0 && (s.charAt(pExp - 1) === '.' || s.charAt(pExp - 1) === ',')) {
                    s = Text.remove(s, pExp - 1, 1);
                    pExp--;
                }
            }
            else {
                var pDot = s.indexOf('.');
                if (pDot === -1) {
                    pDot = s.indexOf(',');
                }
                if (pDot !== -1) {
                    while (s.charAt(s.length - 1) === '0') {
                        s = Text.remove(s, s.length - 1, 1);
                    }
                    if (s.length - 1 === pDot) {
                        s = Text.remove(s, pDot, 1);
                    }
                }
            }
            return s;
        }
        FloatEx.trimFloat = trimFloat;
        ;
        function roundToSignficant(v, digits) {
            var a;
            if (v === 0.0) {
                return 0.0;
            }
            else {
                var exp = Math.floor((Math.log(Math.abs(v)) / Math.LN10)) - digits + 1;
                if (exp >= 0) {
                    a = Math.pow(10, exp);
                    return Math.round(v / a) * a;
                }
                else {
                    a = Math.pow(10, -exp);
                    return Math.round(v * a) / a;
                }
            }
        }
        FloatEx.roundToSignficant = roundToSignficant;
        ;
        function floatToStrSignificant(v, digits, maxLeadingZeros) {
            var bas = 0;
            var q;
            var b;
            var neg;
            var c;
            if (v === 0.0) {
                neg = false;
                q = digits - 1;
                b = 0;
            }
            else {
                neg = v < 0;
                v = Math.abs(v);
                var e = Math.floor(Math.log(v) / Math.LN10);
                var a = Math.pow(10, e - digits + 1);
                b = Math.round(v / a);
                if (b >= Math.pow(10, digits)) {
                    e++;
                }
                q = digits - e - 1;
                if (q < 0 || q >= digits + maxLeadingZeros) {
                    bas = -q + (digits - 1);
                    q = digits - 1;
                }
            }
            var buf = [];
            var t = 0;
            if (bas !== 0) {
                var negBase = bas < 0;
                if (bas < 0) {
                    bas = -bas;
                }
                while (bas !== 0) {
                    c = bas % 10;
                    bas = (bas - c) / 10;
                    buf.push(String.fromCharCode(c + 48));
                }
                if (negBase) {
                    buf.push("-");
                }
                buf.push('E');
            }
            while (b !== 0 || t <= q) {
                c = b % 10;
                b = (b - c) / 10;
                if (t === q && t !== 0) {
                    buf.push('.');
                }
                buf.push(String.fromCharCode(c + 48));
                t++;
            }
            if (neg) {
                buf.push('-');
            }
            return buf.reverse().join("");
        }
        FloatEx.floatToStrSignificant = floatToStrSignificant;
        ;
        function floatFormat(aFloatRes, aDecimalChar) {
            var obj = {
                ftype: 0,
                decimalChar: aDecimalChar || ".",
                decimals: 0,
                fround: 0,
                usesRound: false,
                vsiffror: 0,
                floatRes: $.trim(aFloatRes),
                round: function (v) { return v; },
                convert: null,
                moreAccurate: function (ff) {
                    var x = this.ftype - ff.ftype;
                    if (x > 0) {
                        return true;
                    }
                    else if (x < 0) {
                        return false;
                    }
                    else {
                        switch (this.ftype) {
                            case 0:
                                return true;
                            case 1:
                                return this.vsiffror >= ff.vsiffror;
                            case 2:
                                return this.fround <= ff.fround;
                            default:
                                throw "Unkown FloatFormatType";
                        }
                    }
                }
            };
            if (obj.floatRes.length === 0) {
                obj.ftype = 0;
                obj.usesRound = false;
                obj.convert = function (v) {
                    v = this.round(v);
                    var s = FloatEx.trimFloat(v.toString());
                    if (this.decimalChar !== ".") {
                        s = s.replace(".", this.decimalChar);
                    }
                    return s;
                };
            }
            else {
                if (obj.floatRes.charAt(0) === 'v' || obj.floatRes.charAt(0) === 'V') {
                    obj.ftype = 1;
                    obj.usesRound = true;
                    obj.vsiffror = parseInt(obj.floatRes.substr(1), 10);
                    if (obj.vsiffror > 9 || obj.vsiffror < 1) {
                        obj.vsiffror = 9;
                    }
                    obj.round = function (v) { return FloatEx.roundToSignficant(v, this.vsiffror); };
                    obj.convert = function (v) {
                        var s = FloatEx.floatToStrSignificant(v, this.vsiffror, 4);
                        if (this.decimalChar !== ".") {
                            s = s.replace(".", this.decimalChar);
                        }
                        return s;
                    };
                }
                else {
                    obj.ftype = 2;
                    var b = FloatEx.breakStrFloat(obj.floatRes);
                    if (!b.valid) {
                        throw "Invalid floatRes (" + obj.floatRes + ")";
                    }
                    obj.decimals = b.decAntal;
                    obj.fround = parseFloat(obj.floatRes.replace(/,/g, "."));
                    if (this.fround !== 0.0) {
                        obj.usesRound = true;
                        obj.round = function (v) { return Math.round(v / this.fround) * this.fround; };
                    }
                    obj.convert = function (v) {
                        var s;
                        v = this.round(v);
                        if (this.decimals >= 0) {
                            s = v.toFixed(this.decimals);
                        }
                        else {
                            s = FloatEx.trimFloat(v.toString());
                        }
                        if (this.decimalChar !== ".") {
                            s = s.replace(".", this.decimalChar);
                        }
                        return s;
                    };
                }
            }
            return obj;
        }
        FloatEx.floatFormat = floatFormat;
    })(FloatEx = KeyLib.FloatEx || (KeyLib.FloatEx = {}));
    ;
    var JSONex;
    (function (JSONex) {
        var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/;
        var reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;
        function parseWithDate(json) {
            try {
                var res = JSON.parse(json, function (key, value) {
                    if (typeof value === 'string') {
                        var a = reISO.exec(value);
                        if (a) {
                            return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
                        }
                        a = reMsAjax.exec(value);
                        if (a) {
                            var b = a[1].split(/[-+,.]/);
                            return new Date(b[0] ? (+b[0]) : (0 - (+b[1])));
                        }
                    }
                    return value;
                });
                return res;
            }
            catch (e) {
                throw new Error("JSON content could not be parsed");
            }
        }
        JSONex.parseWithDate = parseWithDate;
        ;
        function dateStringToDate(dtString) {
            var a = reISO.exec(dtString);
            if (a) {
                return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
            }
            a = reMsAjax.exec(dtString);
            if (a) {
                var b = a[1].split(/[-,.]/);
                return new Date(+b[0]);
            }
            return null;
        }
        JSONex.dateStringToDate = dateStringToDate;
        ;
        function stringifyWcf(json) {
            return JSON.stringify(json, function (key, value) {
                if (typeof value === "string") {
                    var a = reISO.exec(value);
                    if (a) {
                        var val = '/Date(' +
                            new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6])).getTime() + ')/';
                        this[key] = val;
                        return val;
                    }
                }
                return value;
            });
        }
        ;
    })(JSONex = KeyLib.JSONex || (KeyLib.JSONex = {}));
    var TimeSpanEx;
    (function (TimeSpanEx) {
        var _timespanEnheter = [
            { txt: "", faktor: 1, aprox: 0 },
            { txt: "ms", faktor: 1, aprox: 0 },
            { txt: "s", faktor: 1000, aprox: 0 },
            { txt: "m", faktor: 60 * 1000, aprox: 90 * 1000 },
            { txt: "t", faktor: 60 * 60 * 1000, aprox: 90 * 60 * 1000 },
            { txt: "h", faktor: 60 * 60 * 1000, aprox: 90 * 60 * 1000 },
            { txt: "d", faktor: 24 * 60 * 60 * 1000, aprox: 48 * 60 * 60 * 1000 }
        ];
        function msek2timeLenStr(msek, addWhiteSpace) {
            var s = "";
            var t;
            if (msek === 0) {
                return "0ms";
            }
            for (t = _timespanEnheter.length - 1; t >= 0; t--) {
                var e = _timespanEnheter[t];
                var fak = e.faktor;
                if (msek >= fak) {
                    var n = Math.floor(msek / fak);
                    msek = Math.round(msek - (n * fak));
                    s += n.toString() + e.txt;
                    if (msek !== 0 && addWhiteSpace) {
                        s += " ";
                    }
                }
            }
            return s;
        }
        TimeSpanEx.msek2timeLenStr = msek2timeLenStr;
        ;
        function msek2approxTimeLenStr(msek, addWhiteSpace) {
            var t;
            for (t = _timespanEnheter.length - 1; t >= 0; t--) {
                var e = _timespanEnheter[t];
                var fak = e.faktor;
                var aprox = e.aprox;
                if (msek >= aprox) {
                    var n = Math.floor((msek + fak / 2) / fak) * fak;
                    if (n === 0) {
                        return addWhiteSpace ? "0 s" : "0s";
                    }
                    else {
                        return msek2timeLenStr(n, addWhiteSpace);
                    }
                }
            }
            return "";
        }
        TimeSpanEx.msek2approxTimeLenStr = msek2approxTimeLenStr;
        ;
        function timeLenStr2msek(s) {
            var state = 0;
            var acc = 0;
            var enhet = "";
            var isDigit = /[0-9]/;
            var isSpace = /[ \t]/;
            var msekSum = 0;
            var c;
            var n;
            var t;
            for (n = 0; n <= s.length; n++) {
                if (n === s.length) {
                    c = "";
                }
                else {
                    c = s.charAt(n);
                }
                statetest: switch (state) {
                    case 0:
                        if (isDigit.test(c)) {
                            acc = 0;
                            enhet = "";
                            state = 1;
                            n--;
                        }
                        else if (!isSpace.test(c) && c !== "") {
                            throw "Invalid timeLenStr, left-crap: " + s;
                        }
                        break;
                    case 1:
                        if (isDigit.test(c)) {
                            acc = acc * 10 + parseInt(c, 10);
                        }
                        else {
                            n--;
                            state = 2;
                        }
                        break;
                    case 2:
                        if (!isSpace.test(c)) {
                            n--;
                            state = 3;
                        }
                        break;
                    case 3:
                        if (isSpace.test(c) || isDigit.test(c) || c === "") {
                            n--;
                            state = 4;
                        }
                        else {
                            enhet += c;
                        }
                        break;
                    case 4:
                        enhet = enhet.toLowerCase();
                        for (t = 0; t < _timespanEnheter.length; t++) {
                            var i = _timespanEnheter[t];
                            if (i.txt === enhet) {
                                msekSum += acc * i.faktor;
                                state = 0;
                                n--;
                                break statetest;
                            }
                        }
                        throw "Invalid timeLenStr unit: " + s;
                    default:
                        throw "impossible";
                }
            }
            return msekSum;
        }
        TimeSpanEx.timeLenStr2msek = timeLenStr2msek;
    })(TimeSpanEx = KeyLib.TimeSpanEx || (KeyLib.TimeSpanEx = {}));
    ;
    function HumanReadableFileSize(n) {
        if (n < 10000) {
            return n.toString();
        }
        else if (n < 10000 * 1024) {
            return Math.round(n / 1024).toString() + " Kb";
        }
        else if (n < 10000 * 1024 * 1024) {
            return Math.round(n / (1024 * 1024)).toString() + " Mb";
        }
        else if (n < 10000 * 1024 * 1024 * 1024) {
            return Math.round(n / (1024 * 1024 * 1024)).toString() + " Gb";
        }
        else {
            return Math.round(n / (1024 * 1024 * 1024 * 1024)).toString() + " Tb";
        }
    }
    KeyLib.HumanReadableFileSize = HumanReadableFileSize;
    ;
    var DateTimeEx;
    (function (DateTimeEx) {
        function isSameDate(d1, d2) {
            return (d1.getFullYear() === d2.getFullYear() &&
                d1.getMonth() === d2.getMonth() &&
                d1.getDate() === d2.getDate());
        }
        DateTimeEx.isSameDate = isSameDate;
        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            return result;
        }
        DateTimeEx.addDays = addDays;
        function justDate(d) {
            var year = d.getFullYear();
            var month = d.getMonth();
            var day = d.getDate();
            return new Date(year, month, day);
        }
        DateTimeEx.justDate = justDate;
        var veckodagar = ['sön', 'mån', 'tis', "ons", "tor", "fre", "lör"];
        var monader = ['jan', 'feb', "mar", "apr", "maj", "jun", "jul", "aug", "sep", "okt", "nov", "dec"];
        function HumanReadableTime(tiddt) {
            var nudt = new Date();
            var delta = nudt.getTime() - tiddt.getTime();
            if (delta < 60 * 60 * 1000) {
                var minuter = Math.floor(delta / (60 * 1000));
                return minuter + " minuter sedan";
            }
            else if (isSameDate(nudt, tiddt)) {
                return "idag " + hhmm100ToString(tiddt.getHours() * 100 + tiddt.getMinutes());
            }
            else if (isSameDate(addDays(nudt, -1), tiddt)) {
                return "ig\u00E5r " + hhmm100ToString(tiddt.getHours() * 100 + tiddt.getMinutes());
            }
            else if (addDays(justDate(nudt), -5).getTime() < justDate(tiddt).getTime()) {
                return veckodagar[tiddt.getDay()] + " " + hhmm100ToString(tiddt.getHours() * 100 + tiddt.getMinutes());
            }
            else if (addDays(justDate(nudt), -290).getTime() < justDate(tiddt).getTime()) {
                return tiddt.getDate() + " " + monader[tiddt.getMonth()] + " " + hhmm100ToString(tiddt.getHours() * 100 + tiddt.getMinutes());
            }
            else {
                return dateToString(tiddt) + " " + hhmm100ToString(tiddt.getHours() * 100 + tiddt.getMinutes());
            }
        }
        DateTimeEx.HumanReadableTime = HumanReadableTime;
        function UTC2local(tid) {
            var x = new Date(tid.getUTCFullYear(), tid.getUTCMonth(), tid.getUTCDate(), tid.getUTCHours(), tid.getUTCMinutes(), tid.getUTCSeconds());
            x.setTime(x.getTime() + daylightSaving(tid) + 3600000);
            return x;
        }
        DateTimeEx.UTC2local = UTC2local;
        ;
        function local2UTC(tid) {
            var x = new Date(tid.getUTCFullYear(), tid.getUTCMonth(), tid.getUTCDate(), tid.getUTCHours(), tid.getUTCMinutes(), tid.getUTCSeconds());
            x.setTime(x.getTime() - daylightSaving(tid) - 3600000);
            return x;
        }
        DateTimeEx.local2UTC = local2UTC;
        ;
        function daylightSaving(tid) {
            return (tid.getTime() > _findDay(tid, 0, 3 - 1, 2, -1).getTime())
                && (tid.getTime() < _findDay(tid, 0, 10 - 1, 2, -1).getTime())
                ? 3600000 : 0;
        }
        DateTimeEx.daylightSaving = daylightSaving;
        ;
        function _findDay(tid, d, m, h, p) {
            var week = (p < 0) ? 7 * (p + 1) : 7 * (p - 1);
            var nm = (p < 0) ? m + 1 : m;
            var x = new Date(tid.getUTCFullYear(), nm, 1, h, 0, 0);
            var dOff = 0;
            if (p < 0) {
                x.setTime(x.getTime() - 86400000);
            }
            if (x.getDay() !== d) {
                dOff = (x.getDay() < d) ? (d - x.getDay()) : -(x.getDay() - d);
                if (p < 0 && dOff > 0) {
                    week -= 7;
                }
                if (p > 0 && dOff < 0) {
                    week += 7;
                }
                x.setTime(x.getTime() + ((dOff + week) * 86400000));
            }
            return x;
        }
        ;
        function dateToString(d) {
            var y = (d.getFullYear()).toString();
            var m = (d.getMonth() + 1).toString();
            var d2 = d.getDate().toString();
            if (m.length === 1) {
                m = "0" + m;
            }
            if (d2.length === 1) {
                d2 = "0" + d2;
            }
            return y + "-" + m + "-" + d2;
        }
        DateTimeEx.dateToString = dateToString;
        ;
        function timeToString(d, inludeSek) {
            if (inludeSek === void 0) { inludeSek = true; }
            var h = (d.getHours()).toString();
            var m = (d.getMinutes()).toString();
            if (h.length === 1) {
                h = "0" + h;
            }
            if (m.length === 1) {
                m = "0" + m;
            }
            if (inludeSek) {
                var s = d.getSeconds().toString();
                if (s.length === 1) {
                    s = "0" + s;
                }
                return h + ":" + m + ":" + s;
            }
            else
                return h + ":" + m;
        }
        DateTimeEx.timeToString = timeToString;
        ;
        function hhmm100ToDate(hhmm100) {
            if (hhmm100 === null) {
                return null;
            }
            else {
                var h = Math.floor(hhmm100 / 100);
                var m = hhmm100 % 100;
                return new Date(0, 0, 0, h, m, 0);
            }
        }
        DateTimeEx.hhmm100ToDate = hhmm100ToDate;
        ;
        function dateTohhmm100(adate) {
            if (adate === null) {
                return null;
            }
            else {
                return adate.getHours() * 100 + adate.getMinutes();
            }
        }
        DateTimeEx.dateTohhmm100 = dateTohhmm100;
        ;
        function hhmm100_to_antalMin(hhmm100) {
            var h = Math.floor(hhmm100 / 100);
            var m = hhmm100 % 100;
            if (m > 59 || m < 0) {
                throw { name: "InvalidData", message: "invalid hhmm100, " + hhmm100 };
            }
            return h * 60 + m;
        }
        DateTimeEx.hhmm100_to_antalMin = hhmm100_to_antalMin;
        ;
        function antalMin_to_hhmm100(hhmm100) {
            var h = Math.floor(hhmm100 / 60);
            var m = hhmm100 % 60;
            return h * 100 + m;
        }
        DateTimeEx.antalMin_to_hhmm100 = antalMin_to_hhmm100;
        ;
        function parsehhmm100(s, allow2400, throwIfOutOfRange) {
            var m = 0;
            var h = 0;
            var st = 0;
            var antal = 0;
            var t = 0;
            var antaldigits = 0;
            var lastWasDig = false;
            var thisIsDig = false;
            var q;
            for (q = 0; q < s.length; q++) {
                thisIsDig = (s[q] >= "0" && s[q] <= "9");
                if (thisIsDig && !lastWasDig) {
                    antaldigits = 1;
                }
                if (thisIsDig && lastWasDig) {
                    antaldigits++;
                }
                lastWasDig = thisIsDig;
            }
            while (t < s.length) {
                var ischar = s[t] >= "0" && s[t] <= "9";
                var n = ischar ? parseInt(s[t], 10) : 0;
                var issep = s[t] === ':';
                var iswhite = s[t] === ' ';
                if (st === 0) {
                    if (iswhite) {
                        t++;
                    }
                    else {
                        st = 1;
                    }
                }
                else if (st === 1) {
                    if (ischar) {
                        h = h * 10 + n;
                        antal++;
                        if (antaldigits > 2 && antal >= antaldigits - 2) {
                            st = 2;
                        }
                        t++;
                    }
                    else if (issep || iswhite) {
                        st = 2;
                    }
                    else {
                        throw "Syntax error";
                    }
                }
                else if (st === 2) {
                    if (issep || iswhite) {
                        t++;
                    }
                    else {
                        st = 3;
                    }
                }
                else if (st === 3) {
                    if (ischar) {
                        m = m * 10 + n;
                        t++;
                    }
                    else if (iswhite) {
                        st = 4;
                    }
                    else {
                        throw "Syntax error";
                    }
                }
                else if (st === 4) {
                    if (iswhite) {
                        t++;
                    }
                    else {
                        throw "Syntax error";
                    }
                }
            }
            var svar = ((h + Math.floor(m / 60)) * 100) + (m % 60);
            if (allow2400 === undefined) {
                allow2400 = true;
            }
            if (svar > (allow2400 ? 2400 : 2359)) {
                if (throwIfOutOfRange === undefined || throwIfOutOfRange) {
                    throw "Syntax error";
                }
            }
            return svar;
        }
        DateTimeEx.parsehhmm100 = parsehhmm100;
        ;
        function hhmm100ToString(hhmm100) {
            var h = Math.floor(hhmm100 / 100);
            var m = Math.round(hhmm100 - (h * 100));
            return (h <= 9 ? "0" : "") + h.toString() + ":" + (m <= 9 ? "0" : "") + m.toString();
        }
        DateTimeEx.hhmm100ToString = hhmm100ToString;
        ;
        function dayOfYear(d) {
            var onejan = new Date(d.getFullYear(), 0, 1);
            return Math.ceil((d.getTime() - onejan.getTime()) / 86400000);
        }
        DateTimeEx.dayOfYear = dayOfYear;
        ;
        function decodeXSDdateTime(s) {
            if (s === undefined) {
                return undefined;
            }
            if (s === null) {
                return null;
            }
            var x = new Date();
            x.setUTCFullYear(parseInt(s.substring(0, 4), 10), parseInt(s.substring(5, 7), 10) - 1, parseInt(s.substring(8, 10), 10));
            x.setUTCHours(parseInt(s.substring(11, 13), 10), parseInt(s.substring(14, 16), 10), parseInt(s.substring(17, 19), 10));
            var mo = (x.getMonth() + 1).toString();
            var da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }
            var hr = x.getHours().toString();
            if (hr.length === 1) {
                hr = "0" + hr;
            }
            var mn = x.getMinutes().toString();
            if (mn.length === 1) {
                mn = "0" + mn;
            }
            var se = x.getSeconds().toString();
            if (se.length === 1) {
                se = "0" + se;
            }
            return x.getFullYear() + "-" + mo + "-" + da + " " + hr + ":" + mn + ":" + se;
        }
        DateTimeEx.decodeXSDdateTime = decodeXSDdateTime;
        ;
        function parseIso8601(CurDate) {
            if (typeof CurDate !== "string" || CurDate === "") {
                return null;
            }
            var S = "[\\-/:.]";
            var Yr = "((?:1[6-9]|[2-9][0-9])[0-9]{2})";
            var Mo = S + "((?:1[012])|(?:0[1-9])|[1-9])";
            var Dy = S + "((?:3[01])|(?:[12][0-9])|(?:0[1-9])|[1-9])";
            var Hr = "(2[0-4]|[01]?[0-9])";
            var Mn = S + "([0-5]?[0-9])";
            var Sd = "(?:" + S + "([0-5]?[0-9])(?:[.,]([0-9]+))?)?";
            var TZ = "(?:(Z)|(?:([\+\-])(1[012]|[0]?[0-9])(?::?([0-5]?[0-9]))?))?";
            var TF = new RegExp("^" + Yr + "(?:" + Mo + "(?:" + Dy + ")?)?" + "$").exec(CurDate);
            if (TF) {
            }
            else {
                TF = TF = new RegExp("^" + Yr + Mo + Dy + "[Tt ]" + Hr + Mn + Sd + TZ + "$").exec(CurDate);
                if (TF) {
                }
            }
            if (!TF) {
                return null;
            }
            if (!TF[2]) {
                TF[2] = 1;
            }
            else {
                TF[2] = TF[2] - 1;
            }
            if (!TF[3]) {
                TF[3] = 1;
            }
            if (!TF[4]) {
                TF[4] = 0;
            }
            if (!TF[5]) {
                TF[5] = 0;
            }
            if (!TF[6]) {
                TF[6] = 0;
            }
            if (!TF[7]) {
                TF[7] = 0;
            }
            if (!TF[8]) {
                TF[8] = null;
            }
            if (TF[9] !== "-" && TF[9] !== "+") {
                TF[9] = null;
            }
            if (!TF[10]) {
                TF[10] = 0;
            }
            else {
                TF[10] = TF[9] + TF[10];
            }
            if (!TF[11]) {
                TF[11] = 0;
            }
            else {
                TF[11] = TF[9] + TF[11];
            }
            if (!TF[8] && !TF[9]) {
                return new Date(TF[1], TF[2], TF[3], TF[4], TF[5], TF[6], TF[7]);
            }
            if (TF[8] === "Z") {
                return new Date(Date.UTC(TF[1], TF[2], TF[3], TF[4], TF[5], TF[6], TF[7]));
            }
            if (TF[9] === "-" || TF[9] === "+") {
                var CurTZ = new Date().getTimezoneOffset();
                var CurTZh = TF[10] - ((CurTZ >= 0 ? -1 : 1) * (Math.floor(Math.abs(CurTZ) / 60)));
                var CurTZm = TF[11] - ((CurTZ >= 0 ? -1 : 1) * (Math.abs(CurTZ) % 60));
                return new Date(TF[1], TF[2], TF[3], TF[4] - CurTZh, TF[5] - CurTZm, TF[6], TF[7]);
            }
            return null;
        }
        DateTimeEx.parseIso8601 = parseIso8601;
        ;
        function iso8601Format(x, includeTime, isUTC) {
            if (includeTime === undefined) {
                includeTime = true;
            }
            var s;
            var mo = (x.getMonth() + 1).toString();
            var da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }
            s = x.getFullYear().toString() + "-" + mo + "-" + da;
            if (includeTime) {
                var hr = x.getHours().toString();
                if (hr.length === 1) {
                    hr = "0" + hr;
                }
                var mn = x.getMinutes().toString();
                if (mn.length === 1) {
                    mn = "0" + mn;
                }
                var se = x.getSeconds().toString();
                if (se.length === 1) {
                    se = "0" + se;
                }
                s = s + "T" + hr + ":" + mn + ":" + se;
                if (isUTC) {
                    s += "Z";
                }
                else {
                    var TimeZoneOffset = x.getTimezoneOffset();
                    var TimeZoneInfo = (TimeZoneOffset >= 0 ? "-" : "+") + ("0" + (Math.floor(Math.abs(TimeZoneOffset) / 60))).slice(-2)
                        + ":" + ("00" + (Math.abs(TimeZoneOffset) % 60)).slice(-2);
                    s += TimeZoneInfo;
                }
            }
            return s;
        }
        DateTimeEx.iso8601Format = iso8601Format;
        function dateTimeToString(x) {
            var s;
            var mo = (x.getMonth() + 1).toString();
            var da = x.getDate().toString();
            if (mo.length === 1) {
                mo = "0" + mo;
            }
            if (da.length === 1) {
                da = "0" + da;
            }
            s = x.getFullYear().toString() + "-" + mo + "-" + da;
            var hr = x.getHours().toString();
            if (hr.length === 1) {
                hr = "0" + hr;
            }
            var mn = x.getMinutes().toString();
            if (mn.length === 1) {
                mn = "0" + mn;
            }
            var se = x.getSeconds().toString();
            if (se.length === 1) {
                se = "0" + se;
            }
            s = s + " " + hr + ":" + mn + ":" + se;
            return s;
        }
        DateTimeEx.dateTimeToString = dateTimeToString;
    })(DateTimeEx = KeyLib.DateTimeEx || (KeyLib.DateTimeEx = {}));
    ;
    var RegExprEx;
    (function (RegExprEx) {
        function wildcardToRegex(wildcard) {
            var s = "";
            var i;
            var lastc = "";
            for (i = 0; i < wildcard.length; i++) {
                var c = wildcard.charAt(i);
                switch (c) {
                    case '*':
                        if (lastc !== "*") {
                            s = s + ".*";
                        }
                        break;
                    case '?':
                        s = s + ".";
                        break;
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '$':
                    case '^':
                    case '.':
                    case '{':
                    case '}':
                    case '|':
                    case '\\':
                        s = s + "\\" + c;
                        break;
                    default:
                        s = s + c;
                        break;
                }
                lastc = c;
            }
            return ("^" + s + "$");
        }
        RegExprEx.wildcardToRegex = wildcardToRegex;
        function makeExakt(str) {
            var s = "";
            var i;
            for (i = 0; i < str.length; i++) {
                var c = str.charAt(i);
                switch (c) {
                    case '^':
                    case '$':
                    case '*':
                    case '?':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '$':
                    case '^':
                    case '.':
                    case '{':
                    case '}':
                    case '|':
                    case '\\':
                        s = s + "\\" + c;
                        break;
                    default:
                        s = s + c;
                        break;
                }
            }
            return s;
        }
        RegExprEx.makeExakt = makeExakt;
    })(RegExprEx = KeyLib.RegExprEx || (KeyLib.RegExprEx = {}));
    ;
    var ScaleTicks;
    (function (ScaleTicks) {
        function createScaleTicks() {
            var data = [
                { major: 0.1, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 1.0, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 0.2, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 0.5, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 0.2, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.4, substeps: 4, Threshold: 3, offset: 0 },
                { major: 0.4, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.6, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 0 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 1 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 2 },
                { major: 0.8, substeps: 4, Threshold: 3, offset: 3 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.15, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.30, substeps: 3, Threshold: 3, offset: 2 },
                { major: 0.25, substeps: 5, Threshold: 3, offset: 0 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 0 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 1 },
                { major: 0.75, substeps: 3, Threshold: 3, offset: 2 }
            ];
            var needCalc = true;
            var fShigh;
            var fSlow;
            var fAproxMajorCount;
            var fstartN;
            var fminor;
            var fmajor;
            var fsubsteps;
            var foffsetMajor;
            var floatRes;
            var decimaler;
            function calcAntalMajor(substeps, major, offset) {
                var minor = major / substeps;
                var N0 = ~~(fSlow / minor);
                var N1 = ~~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~~(N1 / substeps) - ~~((N0 + substeps - 1) / substeps) + 1);
            }
            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }
                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }
                var delta = fShigh - fSlow;
                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }
                function myLog10(n) {
                    return Math.log(n) / Math.LN10;
                }
                var optimal = delta / fAproxMajorCount;
                var expo = (Math.floor(myLog10(Math.abs(optimal)))) + 1;
                var pow = Math.pow(10, expo);
                optimal = optimal / pow;
                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;
                var bestAntal = 1000;
                var bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        var antal = calcAntalMajor(p.substeps, p.major * pow, p.offset);
                        var score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });
                fmajor = fmajor * pow;
                fminor = fmajor / fsubsteps;
                fstartN = ~~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }
                floatRes = fmajor;
                expo = (Math.floor(myLog10(Math.abs(fmajor))));
                if (expo < 0) {
                    decimaler = -expo;
                }
                else {
                    decimaler = 0;
                }
            }
            var obj = {
                setScale: function (slow, shigh) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    var svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v, extradecimaler) {
                            extradecimaler = extradecimaler || 0;
                            return v.toFixed(decimaler + extradecimaler);
                        }
                    };
                    return svar;
                }
            };
            return obj;
        }
        ScaleTicks.createScaleTicks = createScaleTicks;
        ;
        function createScaleTicks_hhmm() {
            var data = [
                { major: 1, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 2, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 5, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 10, substeps: 5, Threshold: 9e99, offset: 0 },
                { major: 15, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 20, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 30, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 60, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 120, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 180, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 240, substeps: 4, Threshold: 9e99, offset: 0 },
                { major: 360, substeps: 3, Threshold: 9e99, offset: 0 },
                { major: 720, substeps: 3, Threshold: 9e99, offset: 0 }
            ];
            var needCalc = true;
            var fShigh;
            var fSlow;
            var fAproxMajorCount;
            var fstartN;
            var fminor;
            var fmajor;
            var fsubsteps;
            var foffsetMajor;
            var floatRes;
            var decimaler;
            function calcAntalMajor(substeps, major, offset) {
                var minor = major / substeps;
                var N0 = ~~(fSlow / minor);
                var N1 = ~~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~~(N1 / substeps) - ~~((N0 + substeps - 1) / substeps) + 1);
            }
            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }
                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }
                var delta = fShigh - fSlow;
                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }
                function myLog10(n) {
                    return Math.log(n) / Math.LN10;
                }
                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;
                var bestAntal = 1000;
                var bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        var antal = calcAntalMajor(p.substeps, p.major, p.offset);
                        var score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });
                fmajor = fmajor;
                fminor = fmajor / fsubsteps;
                fstartN = ~~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }
                floatRes = fmajor;
                decimaler = 0;
            }
            var obj = {
                setScale: function (slow, shigh) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    var svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v, extradecimaler) {
                            extradecimaler = extradecimaler || 0;
                            return DateTimeEx.hhmm100ToString(DateTimeEx.antalMin_to_hhmm100(Math.round(v)));
                        }
                    };
                    return svar;
                }
            };
            return obj;
        }
        ScaleTicks.createScaleTicks_hhmm = createScaleTicks_hhmm;
        ;
        function createScaleTicks_ms() {
            var needCalc = true;
            var fShigh;
            var fSlow;
            var fAproxMajorCount;
            var fstartN;
            var fminor;
            var fmajor;
            var fsubsteps;
            var foffsetMajor;
            function calcAntalMajor(substeps, major, offset) {
                var minor = major / substeps;
                var N0 = ~~(fSlow / minor);
                var N1 = ~~(fShigh / minor);
                if (N0 * minor < fSlow) {
                    N0++;
                }
                if (N1 * minor > fShigh) {
                    N1--;
                }
                N0 += offset;
                N1 += offset;
                return (~~(N1 / substeps) - ~~((N0 + substeps - 1) / substeps) + 1);
            }
            function calc() {
                if (!needCalc) {
                    return;
                }
                needCalc = false;
                if (fShigh < fSlow) {
                    throw "High < low";
                }
                if (fAproxMajorCount < 3) {
                    fAproxMajorCount = 3;
                }
                var delta = fShigh - fSlow;
                if (delta === 0) {
                    fstartN = 0;
                    fminor = 1;
                    fsubsteps = 5;
                    foffsetMajor = 0;
                    return;
                }
                function myLog10(n) {
                    return Math.log(n) / Math.LN10;
                }
                var data = [
                    { major: 10 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 15 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 20 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 30 * 1000, substeps: 3, Threshold: 9e99, offset: 0 },
                    { major: 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 2 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 60 * 1000, substeps: 3, Threshold: 9e99, offset: 0 },
                    { major: 4 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 5 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 10 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 15 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 20 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 30 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 2 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 4 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 6 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 8 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 12 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 24 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 2 * 24 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 3 * 24 * 60 * 60 * 1000, substeps: 6, Threshold: 9e99, offset: 0 },
                    { major: 4 * 24 * 60 * 60 * 1000, substeps: 4, Threshold: 9e99, offset: 0 },
                    { major: 5 * 24 * 60 * 60 * 1000, substeps: 5, Threshold: 9e99, offset: 0 },
                    { major: 7 * 24 * 60 * 60 * 1000, substeps: 7, Threshold: 9e99, offset: 0 }
                ];
                foffsetMajor = 0;
                fmajor = 1;
                fsubsteps = 1;
                var bestAntal = 1000;
                var bestScore = 1000;
                $.each(data, function (i, p) {
                    if (fAproxMajorCount <= p.Threshold) {
                        var antal = calcAntalMajor(p.substeps, p.major, p.offset);
                        var score = Math.abs(antal - fAproxMajorCount);
                        if (antal < 3) {
                            score += (3 - antal) * 100;
                        }
                        if (score < bestScore || (score === bestScore && bestAntal === 2 && antal === 4)) {
                            bestAntal = antal;
                            bestScore = score;
                            fmajor = p.major;
                            fsubsteps = p.substeps;
                            foffsetMajor = p.offset;
                        }
                    }
                });
                fmajor = fmajor;
                fminor = fmajor / fsubsteps;
                fstartN = ~~(fSlow / fminor);
                if (fstartN * fminor < fSlow) {
                    fstartN++;
                }
            }
            var obj = {
                setScale: function (slow, shigh) {
                    if (fShigh !== shigh || fSlow !== slow) {
                        fShigh = shigh;
                        fSlow = slow;
                        needCalc = true;
                    }
                },
                getScaleHigh: function () {
                    return fShigh;
                },
                getScaleLow: function () {
                    return fSlow;
                },
                setAproxMajorCount: function (ny) {
                    if (fAproxMajorCount !== ny) {
                        fAproxMajorCount = ny;
                        needCalc = true;
                    }
                },
                getAproxMajorCount: function () {
                    return fAproxMajorCount;
                },
                getDrawData: function () {
                    calc();
                    var svar = {
                        startN: fstartN,
                        minor: fminor,
                        major: fmajor,
                        subSteps: fsubsteps,
                        offsetMajor: foffsetMajor,
                        formatValue: function (v) {
                            if (v === 0) {
                                return "0";
                            }
                            else {
                                return TimeSpanEx.msek2timeLenStr(v, false);
                            }
                        }
                    };
                    return svar;
                }
            };
            return obj;
        }
        ScaleTicks.createScaleTicks_ms = createScaleTicks_ms;
    })(ScaleTicks = KeyLib.ScaleTicks || (KeyLib.ScaleTicks = {}));
    ;
    var Coord;
    (function (Coord) {
        var RectangleF = (function () {
            function RectangleF(r) {
                this.x = r.x;
                this.y = r.y;
                this.w = r.w;
                this.h = r.h;
            }
            RectangleF.prototype.right = function () {
                return this.x + this.w;
            };
            RectangleF.prototype.bottom = function () {
                return this.y + this.h;
            };
            RectangleF.prototype.centerY = function () {
                return this.y + this.h / 2;
            };
            RectangleF.prototype.centerX = function () {
                return this.x + this.w / 2;
            };
            RectangleF.prototype.contains = function (p) {
                return p.x >= this.x && p.y >= this.y && p.x < this.right() && p.y < this.bottom();
            };
            RectangleF.prototype.union = function (r) {
                if (r) {
                    var left = Math.min(this.x, r.x);
                    var top_1 = Math.min(this.y, r.y);
                    var right = Math.max(this.x + this.w, r.x + r.w);
                    var bottom = Math.max(this.y + this.h, r.y + r.h);
                    return new RectangleF({
                        x: left,
                        y: top_1,
                        w: right - left,
                        h: bottom - top_1
                    });
                }
                else {
                    return new RectangleF(this);
                }
            };
            return RectangleF;
        }());
        Coord.RectangleF = RectangleF;
    })(Coord = KeyLib.Coord || (KeyLib.Coord = {}));
    var TEST;
    (function (TEST) {
        function floatex_floatToStrSignificant() {
            var ok = true;
            function test(v, digits, maxLeadingZeros, svar) {
                var r = FloatEx.floatToStrSignificant(v, digits, maxLeadingZeros);
                if (r !== svar) {
                    console.log("floatToStrSignificant(" + v + "," + digits + "," + maxLeadingZeros +
                        ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(1.234567, 4, 2, "1.235");
            test(1.234567, 3, 2, "1.23");
            test(1, 3, 2, "1.00");
            test(0, 3, 2, "0.00");
            test(56789, 3, 2, "5.68E4");
            test(-56789, 3, 2, "-5.68E4");
            test(0.012345, 2, 2, "0.012");
            test(0.0012345, 2, 2, "1.2E-3");
            test(0.00012345, 2, 2, "1.2E-4");
            return ok;
        }
        ;
        function floatex_roundToSignficant() {
            var ok = true;
            function test(v, digits, svar) {
                var r = FloatEx.roundToSignficant(v, digits);
                if (r !== svar) {
                    console.log("roundToSignficant(" + v + "," + digits + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(0.1234, 2, 0.12);
            test(-0.1234, 3, -0.123);
            test(0.123456, 4, 0.1235);
            test(56.789, 4, 56.79);
            test(0, 10, 0);
            test(12345, 2, 12000);
            test(-12345, 2, -12000);
            return ok;
        }
        ;
        function floatex_trimFloat() {
            var ok = true;
            function test(s, svar) {
                var r = FloatEx.trimFloat(s);
                if (r !== svar) {
                    console.log("trimFloat(" + s + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test("0.100", "0.1");
            test("1.00E009", "1E9");
            test("0", "0");
            test("1.2", "1.2");
            test("1.0", "1");
            test("1\xa0000.0", "1000");
            return ok;
        }
        ;
        function floatex_floatFormat() {
            var ok = true;
            function test(s, v, svar, c) {
                var ff = FloatEx.floatFormat(s, c);
                var r = ff.convert(v);
                if (r !== svar) {
                    console.log("floatFormat(" + s + ").convert(" + v + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test("1.00", 2, "2.00", ".");
            test("1.00", 2, "2,00", ",");
            test("V3", 12345, "1.23E4", ".");
            test("V2", 1.2345, "1.2", ".");
            test("0.25", 4.567, "4.50", ".");
            return ok;
        }
        ;
        function DateTimeEx_hhmm100_to_antalMin() {
            var ok = true;
            function test(n, svar) {
                var r = DateTimeEx.hhmm100_to_antalMin(n);
                if (r !== svar) {
                    console.log("hhmm100_to_antalMin(" + r + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(900, 9 * 60);
            test(1730, 17 * 60 + 30);
            return ok;
        }
        ;
        function DateTimeEx_antalMin_to_hhmm100() {
            var ok = true;
            function test(n, svar) {
                var r = DateTimeEx.antalMin_to_hhmm100(n);
                if (r !== svar) {
                    console.log("antalMin_to_hhmm100(" + r + ") gav " + r + ", förväntade " + svar);
                    ok = false;
                }
            }
            test(9 * 60, 900);
            test(17 * 60 + 30, 1730);
        }
        ;
        function testTextSort() {
            var ok = true;
            var test = function (s, n, l) {
                var x = Text.findNumeric(s);
                if ((x.index !== n) || (x.len !== l)) {
                    console.log("Text.findNumerix('" + s + "') gav " + JSON.stringify(x) + ", förväntade "
                        + JSON.stringify({ index: n, len: l }));
                    ok = false;
                }
            };
            test('123', 0, 3);
            test('12abc123', 0, 2);
            test('abc123', 3, 3);
            test('abc123x', 3, 3);
            test('hejhej', 6, 0);
            test('', -1, 0);
            var testC = function (s1, s2, svar) {
                var x = Text.compareStringWithNumeric(s1, s2);
                x = x < 0 ? -1 : (x > 0 ? 1 : 0);
                if (x !== svar) {
                    console.log("Text.compareStringWithNumeric('" + s1 + "','" + s2 + "') gav " + x + ", borde vara " + svar);
                    ok = false;
                }
            };
            testC('test', 'allo', 1);
            testC('allo', 'test', -1);
            testC('test', 'test', 0);
            testC('123', '015', 1);
            testC('015', '123', -1);
            testC('015', '015', 0);
            testC('123', '15', 1);
            testC('15', '123', -1);
            testC('15', '015', 0);
            testC('hej8', 'hej9', -1);
            testC('hej9', 'hej10', -1);
            testC('hej8allå', 'hej9allå', -1);
            testC('hej9allå', 'hej10allå', -1);
            testC('hej8allåA', 'hej8allåB', -1);
            testC('hej008allåA', 'hej8allåB', -1);
            testC('hej8allå', 'hej8allåB', -1);
            testC('8allå', '8allåB', -1);
            testC('hek8', 'hej9', 1);
            testC('hej8hej8', 'hej8hek8', -1);
            return ok;
        }
        function testAll() {
            return testTextSort() &&
                floatex_floatToStrSignificant() &&
                floatex_roundToSignficant() &&
                floatex_floatFormat() &&
                floatex_trimFloat() &&
                DateTimeEx_hhmm100_to_antalMin() &&
                DateTimeEx_antalMin_to_hhmm100();
        }
        TEST.testAll = testAll;
    })(TEST = KeyLib.TEST || (KeyLib.TEST = {}));
})(KeyLib || (KeyLib = {}));
//# sourceMappingURL=keyLib2.js.map